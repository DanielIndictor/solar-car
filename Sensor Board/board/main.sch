EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 2
Title "Solar Car Telemetry System"
Date "2020-03-12"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+3.3V #PWR022
U 1 1 5E5C2C29
P 4200 3150
AR Path="/5E5C2C29" Ref="#PWR022"  Part="1" 
AR Path="/5E28CC93/5E5C2C29" Ref="#PWR?"  Part="1" 
F 0 "#PWR022" H 4200 3000 50  0001 C CNN
F 1 "+3.3V" H 4215 3323 50  0000 C CNN
F 2 "" H 4200 3150 50  0001 C CNN
F 3 "" H 4200 3150 50  0001 C CNN
	1    4200 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5E5C2C2F
P 4150 4200
AR Path="/5E5C2C2F" Ref="#PWR018"  Part="1" 
AR Path="/5E28CC93/5E5C2C2F" Ref="#PWR?"  Part="1" 
F 0 "#PWR018" H 4150 3950 50  0001 C CNN
F 1 "GND" H 4155 4027 50  0000 C CNN
F 2 "" H 4150 4200 50  0001 C CNN
F 3 "" H 4150 4200 50  0001 C CNN
	1    4150 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4150 4150 4150
Wire Wire Line
	4150 4150 4150 4200
Text Label 4100 3750 0    50   ~ 0
MISO
Text Label 4100 3650 0    50   ~ 0
MOSI
Text Label 4100 3550 0    50   ~ 0
SCK
$Comp
L power:GNDD #PWR013
U 1 1 5E5C2C41
P 2200 4250
AR Path="/5E5C2C41" Ref="#PWR013"  Part="1" 
AR Path="/5E28CC93/5E5C2C41" Ref="#PWR?"  Part="1" 
F 0 "#PWR013" H 2200 4000 50  0001 C CNN
F 1 "GNDD" H 2204 4095 50  0000 C CNN
F 2 "" H 2200 4250 50  0001 C CNN
F 3 "" H 2200 4250 50  0001 C CNN
	1    2200 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal_Small Y1
U 1 1 5E5C2C47
P 4650 3850
AR Path="/5E5C2C47" Ref="Y1"  Part="1" 
AR Path="/5E28CC93/5E5C2C47" Ref="Y?"  Part="1" 
F 0 "Y1" V 4650 3900 50  0000 R CNN
F 1 "4.096MHz" V 4650 4300 50  0000 R CNN
F 2 "Crystal:Crystal_HC49-U_Vertical" H 4650 3850 50  0001 C CNN
F 3 "http://www.ecsxtal.com/store/pdf/hc49ux.pdf" H 4650 3850 50  0001 C CNN
	1    4650 3850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C21
U 1 1 5E5C2C4D
P 5300 4050
AR Path="/5E5C2C4D" Ref="C21"  Part="1" 
AR Path="/5E28CC93/5E5C2C4D" Ref="C?"  Part="1" 
F 0 "C21" V 5071 4050 50  0000 C CNN
F 1 "22pF" V 5162 4050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5300 4050 50  0001 C CNN
F 3 "https://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2018/06/11/CL21C470JC61PNC.pdf" H 5300 4050 50  0001 C CNN
	1    5300 4050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5E5C2C53
P 5400 4200
AR Path="/5E5C2C53" Ref="#PWR027"  Part="1" 
AR Path="/5E28CC93/5E5C2C53" Ref="#PWR?"  Part="1" 
F 0 "#PWR027" H 5400 3950 50  0001 C CNN
F 1 "GND" H 5405 4027 50  0000 C CNN
F 2 "" H 5400 4200 50  0001 C CNN
F 3 "" H 5400 4200 50  0001 C CNN
	1    5400 4200
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:LM2596S-3.3 U1
U 1 1 5E5C2C59
P 1750 6950
AR Path="/5E5C2C59" Ref="U1"  Part="1" 
AR Path="/5E28CC93/5E5C2C59" Ref="U?"  Part="1" 
F 0 "U1" H 1750 7317 50  0000 C CNN
F 1 "LM2596S-3.3" H 1750 7226 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-5_TabPin3" H 1800 6700 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2596.pdf" H 1750 6950 50  0001 C CNN
	1    1750 6950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR017
U 1 1 5E5C2C5F
P 2900 6750
AR Path="/5E5C2C5F" Ref="#PWR017"  Part="1" 
AR Path="/5E28CC93/5E5C2C5F" Ref="#PWR?"  Part="1" 
F 0 "#PWR017" H 2900 6600 50  0001 C CNN
F 1 "+3.3V" H 2915 6923 50  0000 C CNN
F 2 "" H 2900 6750 50  0001 C CNN
F 3 "" H 2900 6750 50  0001 C CNN
	1    2900 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5E5C2C65
P 700 7050
AR Path="/5E5C2C65" Ref="#PWR04"  Part="1" 
AR Path="/5E28CC93/5E5C2C65" Ref="#PWR?"  Part="1" 
F 0 "#PWR04" H 700 6800 50  0001 C CNN
F 1 "GND" H 705 6877 50  0000 C CNN
F 2 "" H 700 7050 50  0001 C CNN
F 3 "" H 700 7050 50  0001 C CNN
	1    700  7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 7250 1750 7250
$Comp
L pspice:INDUCTOR L1
U 1 1 5E5C2C6C
P 2550 7050
AR Path="/5E5C2C6C" Ref="L1"  Part="1" 
AR Path="/5E28CC93/5E5C2C6C" Ref="L?"  Part="1" 
F 0 "L1" H 2550 7265 50  0000 C CNN
F 1 "33µH@3A" H 2550 7174 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D27.9mm_P20.07mm_Vishay_IHB-3" H 2550 7050 50  0001 C CNN
F 3 "https://www.digikey.com/product-detail/en/bourns-inc/2100LL-330-H-RC/M1406-ND/725953" H 2550 7050 50  0001 C CNN
	1    2550 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6850 2250 6750
Wire Wire Line
	2250 6750 2900 6750
Wire Wire Line
	2900 6750 2900 7050
Connection ~ 2900 6750
Wire Wire Line
	1750 7250 2300 7250
Connection ~ 1750 7250
$Comp
L Device:D_Schottky_Small D5
U 1 1 5E5C2C78
P 2300 7150
AR Path="/5E5C2C78" Ref="D5"  Part="1" 
AR Path="/5E28CC93/5E5C2C78" Ref="D?"  Part="1" 
F 0 "D5" V 2254 7218 50  0000 L CNN
F 1 "STPS5L25" V 2345 7218 50  0000 L CNN
F 2 "Diode_THT:D_DO-201AD_P12.70mm_Horizontal" V 2300 7150 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/68/39/58/a7/fe/b4/45/ac/CD00000845.pdf/files/CD00000845.pdf/jcr:content/translations/en.CD00000845.pdf" V 2300 7150 50  0001 C CNN
	1    2300 7150
	0    1    1    0   
$EndComp
Connection ~ 2300 7250
Wire Wire Line
	2300 7250 2900 7250
$Comp
L Device:CP_Small C16
U 1 1 5E5C2C98
P 2900 7150
AR Path="/5E5C2C98" Ref="C16"  Part="1" 
AR Path="/5E28CC93/5E5C2C98" Ref="C?"  Part="1" 
F 0 "C16" H 2991 7197 50  0000 L CNN
F 1 "470µF@50V" H 2991 7104 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D13.0mm_P5.00mm" H 2900 7150 50  0001 C CNN
F 3 "~" H 2900 7150 50  0001 C CNN
	1    2900 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C1
U 1 1 5E5C2C9E
P 700 6950
AR Path="/5E5C2C9E" Ref="C1"  Part="1" 
AR Path="/5E28CC93/5E5C2C9E" Ref="C?"  Part="1" 
F 0 "C1" H 791 6997 50  0000 L CNN
F 1 "470µF@50V" H 791 6904 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D13.0mm_P5.00mm" H 700 6950 50  0001 C CNN
F 3 "~" H 700 6950 50  0001 C CNN
	1    700  6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 7050 1250 7250
Wire Wire Line
	1250 7050 700  7050
Connection ~ 1250 7050
Connection ~ 700  7050
Wire Notes Line
	3500 7600 3500 6500
Wire Wire Line
	2200 4250 2650 4250
$Comp
L Device:C_Small C12
U 1 1 5E5C2CAB
P 2650 4150
AR Path="/5E5C2CAB" Ref="C12"  Part="1" 
AR Path="/5E28CC93/5E5C2CAB" Ref="C?"  Part="1" 
F 0 "C12" H 2742 4197 50  0000 L CNN
F 1 "100nF" H 2742 4104 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 4150 50  0001 C CNN
F 3 "~" H 2650 4150 50  0001 C CNN
	1    2650 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C6
U 1 1 5E5C2CB1
P 2200 4150
AR Path="/5E5C2CB1" Ref="C6"  Part="1" 
AR Path="/5E28CC93/5E5C2CB1" Ref="C?"  Part="1" 
F 0 "C6" H 2292 4197 50  0000 L CNN
F 1 "4.7µF" H 2292 4104 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2200 4150 50  0001 C CNN
F 3 "~" H 2200 4150 50  0001 C CNN
	1    2200 4150
	1    0    0    -1  
$EndComp
Connection ~ 2650 4050
Wire Wire Line
	2650 4050 2200 4050
$Comp
L power:GNDD #PWR011
U 1 1 5E5C2CB9
P 1950 2950
AR Path="/5E5C2CB9" Ref="#PWR011"  Part="1" 
AR Path="/5E28CC93/5E5C2CB9" Ref="#PWR?"  Part="1" 
F 0 "#PWR011" H 1950 2700 50  0001 C CNN
F 1 "GNDD" H 1955 2794 50  0000 C CNN
F 2 "" H 1950 2950 50  0001 C CNN
F 3 "" H 1950 2950 50  0001 C CNN
	1    1950 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D3
U 1 1 5E5C2CBF
P 1600 3950
AR Path="/5E5C2CBF" Ref="D3"  Part="1" 
AR Path="/5E28CC93/5E5C2CBF" Ref="D?"  Part="1" 
F 0 "D3" V 1600 4050 50  0000 L CNN
F 1 "BAS16LT1G " V 1800 4000 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1600 3950 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1600 3950 50  0001 C CNN
	1    1600 3950
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D1
U 1 1 5E5C2CC5
P 1150 3950
AR Path="/5E5C2CC5" Ref="D1"  Part="1" 
AR Path="/5E28CC93/5E5C2CC5" Ref="D?"  Part="1" 
F 0 "D1" V 1150 3850 50  0000 R CNN
F 1 "BAS16LT1G " V 950 3900 50  0000 R CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1150 3950 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1150 3950 50  0001 C CNN
	1    1150 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 4050 1150 4050
Wire Wire Line
	1600 3850 1150 3850
Wire Wire Line
	2650 4050 3100 4050
$Comp
L Device:C_Small C2
U 1 1 5E5C2CCE
P 900 4050
AR Path="/5E5C2CCE" Ref="C2"  Part="1" 
AR Path="/5E28CC93/5E5C2CCE" Ref="C?"  Part="1" 
F 0 "C2" V 668 4050 50  0000 C CNN
F 1 "33nF" V 761 4050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 900 4050 50  0001 C CNN
F 3 "~" H 900 4050 50  0001 C CNN
	1    900  4050
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 4050 1000 4050
Connection ~ 1150 4050
$Comp
L power:GNDD #PWR01
U 1 1 5E5C2CD6
P 700 4250
AR Path="/5E5C2CD6" Ref="#PWR01"  Part="1" 
AR Path="/5E28CC93/5E5C2CD6" Ref="#PWR?"  Part="1" 
F 0 "#PWR01" H 700 4000 50  0001 C CNN
F 1 "GNDD" H 704 4095 50  0000 C CNN
F 2 "" H 700 4250 50  0001 C CNN
F 3 "" H 700 4250 50  0001 C CNN
	1    700  4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4050 2000 4050
Wire Wire Line
	2000 4050 2000 3950
Wire Wire Line
	2000 3950 3100 3950
Connection ~ 1600 4050
Connection ~ 2200 2950
Wire Wire Line
	1950 2950 2200 2950
$Comp
L Device:C_Small C20
U 1 1 5E5C2CE2
P 5300 3650
AR Path="/5E5C2CE2" Ref="C20"  Part="1" 
AR Path="/5E28CC93/5E5C2CE2" Ref="C?"  Part="1" 
F 0 "C20" V 5071 3650 50  0000 C CNN
F 1 "22pF" V 5162 3650 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5300 3650 50  0001 C CNN
F 3 "https://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2018/06/11/CL21C470JC61PNC.pdf" H 5300 3650 50  0001 C CNN
	1    5300 3650
	0    1    1    0   
$EndComp
Text Notes 1300 3700 0    50   ~ 0
Array shunt and main\nvoltage measurement
$Comp
L power:GND #PWR025
U 1 1 5E5C2CE9
P 4550 3300
AR Path="/5E5C2CE9" Ref="#PWR025"  Part="1" 
AR Path="/5E28CC93/5E5C2CE9" Ref="#PWR?"  Part="1" 
F 0 "#PWR025" H 4550 3050 50  0001 C CNN
F 1 "GND" H 4555 3127 50  0000 C CNN
F 2 "" H 4550 3300 50  0001 C CNN
F 3 "" H 4550 3300 50  0001 C CNN
	1    4550 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E5C2CEF
P 2200 3350
AR Path="/5E5C2CEF" Ref="R1"  Part="1" 
AR Path="/5E28CC93/5E5C2CEF" Ref="R?"  Part="1" 
F 0 "R1" V 2100 3350 50  0000 C CNN
F 1 "249K" V 2200 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2200 3350 50  0001 C CNN
F 3 "CMF55249K00BEEB " H 2200 3350 50  0001 C CNN
	1    2200 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5E5C2CF5
P 2500 3350
AR Path="/5E5C2CF5" Ref="R3"  Part="1" 
AR Path="/5E28CC93/5E5C2CF5" Ref="R?"  Part="1" 
F 0 "R3" V 2400 3350 50  0000 C CNN
F 1 "1K" V 2500 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2500 3350 50  0001 C CNN
F 3 " CMF551K0000BEEB " H 2500 3350 50  0001 C CNN
	1    2500 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 3850 3100 3850
Connection ~ 2200 4250
Wire Wire Line
	2650 4250 3100 4250
Wire Wire Line
	3100 4250 3100 4150
Connection ~ 2650 4250
Wire Wire Line
	1150 4050 1150 4350
Wire Wire Line
	1150 3850 1150 3650
Wire Wire Line
	3100 2750 3100 3200
Wire Wire Line
	3000 2950 3000 3300
Wire Wire Line
	3000 3300 3100 3300
Wire Wire Line
	2900 3150 2900 3450
Wire Wire Line
	2900 3450 3100 3450
Connection ~ 1600 3850
Wire Wire Line
	2650 3350 2650 3400
Wire Wire Line
	2650 3350 2800 3350
Wire Wire Line
	2800 3550 3100 3550
Wire Wire Line
	2800 3350 2800 3550
Wire Wire Line
	3100 3550 3100 3650
Wire Wire Line
	2350 3350 2350 3750
Wire Wire Line
	2350 3750 3100 3750
Connection ~ 2350 3350
Connection ~ 2650 3350
$Comp
L Device:C_Small C18
U 1 1 5E5C2D4D
P 4450 3200
AR Path="/5E5C2D4D" Ref="C18"  Part="1" 
AR Path="/5E28CC93/5E5C2D4D" Ref="C?"  Part="1" 
F 0 "C18" V 4221 3200 50  0000 C CNN
F 1 "100nF" V 4312 3200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4450 3200 50  0001 C CNN
F 3 "~" H 4450 3200 50  0001 C CNN
	1    4450 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 3200 4200 3200
Connection ~ 4200 3200
Wire Wire Line
	4200 3200 4200 3150
Wire Wire Line
	4100 3200 4200 3200
Wire Wire Line
	4550 3300 4550 3200
Wire Wire Line
	4100 3300 4550 3300
Connection ~ 5400 4050
Wire Wire Line
	5400 4050 5400 4200
Wire Wire Line
	5400 3650 5400 4050
Wire Wire Line
	4100 3850 4450 3850
Wire Wire Line
	4450 3850 4450 3750
Wire Wire Line
	4450 3750 4650 3750
Wire Wire Line
	4650 3950 4950 3950
Wire Wire Line
	4950 3950 4950 4050
Wire Wire Line
	4950 4050 5200 4050
Connection ~ 4650 3950
Wire Wire Line
	4650 3750 4950 3750
Wire Wire Line
	4950 3750 4950 3650
Wire Wire Line
	4950 3650 5200 3650
Connection ~ 4650 3750
Wire Wire Line
	2800 7050 2900 7050
Connection ~ 2900 7050
Wire Wire Line
	2250 7050 2300 7050
Connection ~ 2300 7050
Connection ~ 4550 3300
Text Label 4100 4050 0    50   ~ 0
ADE7912_CLKOUT
Wire Wire Line
	4100 3950 4650 3950
$Comp
L main:ADE7912 U3
U 1 1 5E5C2D7E
P 3600 5550
AR Path="/5E5C2D7E" Ref="U3"  Part="1" 
AR Path="/5E28CC93/5E5C2D7E" Ref="U?"  Part="1" 
F 0 "U3" H 3600 6315 50  0000 C CNN
F 1 "ADE7912" H 3600 6224 50  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 3550 4800 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADE7912_7913.pdf" H 3550 4800 50  0001 C CNN
	1    3600 5550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR023
U 1 1 5E5C2D84
P 4200 5000
AR Path="/5E5C2D84" Ref="#PWR023"  Part="1" 
AR Path="/5E28CC93/5E5C2D84" Ref="#PWR?"  Part="1" 
F 0 "#PWR023" H 4200 4850 50  0001 C CNN
F 1 "+3.3V" H 4215 5173 50  0000 C CNN
F 2 "" H 4200 5000 50  0001 C CNN
F 3 "" H 4200 5000 50  0001 C CNN
	1    4200 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5E5C2D8A
P 4150 6050
AR Path="/5E5C2D8A" Ref="#PWR019"  Part="1" 
AR Path="/5E28CC93/5E5C2D8A" Ref="#PWR?"  Part="1" 
F 0 "#PWR019" H 4150 5800 50  0001 C CNN
F 1 "GND" H 4155 5877 50  0000 C CNN
F 2 "" H 4150 6050 50  0001 C CNN
F 3 "" H 4150 6050 50  0001 C CNN
	1    4150 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 6000 4150 6000
Wire Wire Line
	4150 6000 4150 6050
Text Label 4100 5600 0    50   ~ 0
MISO
Text Label 4100 5500 0    50   ~ 0
MOSI
Text Label 4100 5400 0    50   ~ 0
SCK
$Comp
L power:GNDD #PWR014
U 1 1 5E5C2D95
P 2200 6100
AR Path="/5E5C2D95" Ref="#PWR014"  Part="1" 
AR Path="/5E28CC93/5E5C2D95" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 2200 5850 50  0001 C CNN
F 1 "GNDD" H 2204 5945 50  0000 C CNN
F 2 "" H 2200 6100 50  0001 C CNN
F 3 "" H 2200 6100 50  0001 C CNN
	1    2200 6100
	1    0    0    -1  
$EndComp
Connection ~ 2650 5900
$Comp
L Device:D_Small D4
U 1 1 5E5C2D9C
P 1600 5800
AR Path="/5E5C2D9C" Ref="D4"  Part="1" 
AR Path="/5E28CC93/5E5C2D9C" Ref="D?"  Part="1" 
F 0 "D4" V 1600 5900 50  0000 L CNN
F 1 "BAS16LT1G " V 1800 5850 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1600 5800 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1600 5800 50  0001 C CNN
	1    1600 5800
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D2
U 1 1 5E5C2DA2
P 1150 5800
AR Path="/5E5C2DA2" Ref="D2"  Part="1" 
AR Path="/5E28CC93/5E5C2DA2" Ref="D?"  Part="1" 
F 0 "D2" V 1150 5700 50  0000 R CNN
F 1 "BAS16LT1G " V 950 5750 50  0000 R CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1150 5800 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1150 5800 50  0001 C CNN
	1    1150 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 5900 1150 5900
Wire Wire Line
	1600 5700 1150 5700
Wire Wire Line
	2650 5900 3100 5900
Connection ~ 1150 5900
Wire Wire Line
	1600 5900 2000 5900
Wire Wire Line
	2000 5900 2000 5800
Wire Wire Line
	2000 5800 3100 5800
Connection ~ 1600 5900
Text Notes 3050 6450 0    50   Italic 0
*V2P only available for ADE7913
$Comp
L power:GND #PWR026
U 1 1 5E5C2DB1
P 4550 5150
AR Path="/5E5C2DB1" Ref="#PWR026"  Part="1" 
AR Path="/5E28CC93/5E5C2DB1" Ref="#PWR?"  Part="1" 
F 0 "#PWR026" H 4550 4900 50  0001 C CNN
F 1 "GND" H 4555 4977 50  0000 C CNN
F 2 "" H 4550 5150 50  0001 C CNN
F 3 "" H 4550 5150 50  0001 C CNN
	1    4550 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 5700 3100 5700
Wire Wire Line
	2650 6100 3100 6100
Wire Wire Line
	3100 6100 3100 6000
Connection ~ 2650 6100
Wire Wire Line
	1150 5900 1150 6200
Connection ~ 1150 5700
Wire Wire Line
	3100 4600 3100 5050
Wire Wire Line
	3000 4800 3000 5150
Wire Wire Line
	3000 5150 3100 5150
Wire Wire Line
	2900 5000 2900 5300
Wire Wire Line
	2900 5300 3100 5300
Connection ~ 1600 5700
Wire Wire Line
	2800 5400 3100 5400
$Comp
L power:GNDD #PWR016
U 1 1 5E5C2DC5
P 2650 5250
AR Path="/5E5C2DC5" Ref="#PWR016"  Part="1" 
AR Path="/5E28CC93/5E5C2DC5" Ref="#PWR?"  Part="1" 
F 0 "#PWR016" H 2650 5000 50  0001 C CNN
F 1 "GNDD" H 2650 5100 50  0000 C CNN
F 2 "" H 2650 5250 50  0001 C CNN
F 3 "" H 2650 5250 50  0001 C CNN
	1    2650 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 5400 3100 5500
Wire Wire Line
	2350 5600 3100 5600
$Comp
L Device:C_Small C19
U 1 1 5E5C2DCD
P 4450 5050
AR Path="/5E5C2DCD" Ref="C19"  Part="1" 
AR Path="/5E28CC93/5E5C2DCD" Ref="C?"  Part="1" 
F 0 "C19" V 4221 5050 50  0000 C CNN
F 1 "100nF" V 4312 5050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4450 5050 50  0001 C CNN
F 3 "~" H 4450 5050 50  0001 C CNN
	1    4450 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 5050 4200 5050
Connection ~ 4200 5050
Wire Wire Line
	4200 5050 4200 5000
Wire Wire Line
	4100 5050 4200 5050
Wire Wire Line
	4550 5150 4550 5050
Wire Wire Line
	4100 5150 4550 5150
Connection ~ 4550 5150
Text Label 4100 5800 0    50   ~ 0
ADE7912_CLKOUT
Text Notes 1000 4950 0    50   ~ 0
Motor shunt and \nunassigned
$Comp
L Device:CP_Small C5
U 1 1 5E5C2DDC
P 2200 3050
AR Path="/5E5C2DDC" Ref="C5"  Part="1" 
AR Path="/5E28CC93/5E5C2DDC" Ref="C?"  Part="1" 
F 0 "C5" H 2000 3000 50  0000 L CNN
F 1 "4.7µF" H 1900 3100 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2200 3050 50  0001 C CNN
F 3 "~" H 2200 3050 50  0001 C CNN
	1    2200 3050
	-1   0    0    1   
$EndComp
$Comp
L Device:CP_Small C4
U 1 1 5E5C2DE2
P 2200 2850
AR Path="/5E5C2DE2" Ref="C4"  Part="1" 
AR Path="/5E28CC93/5E5C2DE2" Ref="C?"  Part="1" 
F 0 "C4" H 2292 2897 50  0000 L CNN
F 1 "10µF" H 2292 2804 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2200 2850 50  0001 C CNN
F 3 "~" H 2200 2850 50  0001 C CNN
	1    2200 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2950 3000 2950
Wire Wire Line
	2650 2950 2200 2950
Connection ~ 2650 2950
Wire Wire Line
	2650 3150 2900 3150
Wire Wire Line
	2200 3150 2650 3150
Connection ~ 2650 3150
$Comp
L Device:C_Small C11
U 1 1 5E5C2DEE
P 2650 3050
AR Path="/5E5C2DEE" Ref="C11"  Part="1" 
AR Path="/5E28CC93/5E5C2DEE" Ref="C?"  Part="1" 
F 0 "C11" H 2742 3097 50  0000 L CNN
F 1 "100nF" H 2742 3004 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 3050 50  0001 C CNN
F 3 "~" H 2650 3050 50  0001 C CNN
	1    2650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5200 2350 5600
Wire Wire Line
	2800 5200 2800 5400
Wire Wire Line
	2650 5200 2800 5200
Wire Wire Line
	2650 5200 2650 5250
Connection ~ 2650 5200
$Comp
L Device:R R4
U 1 1 5E5C2DF9
P 2500 5200
AR Path="/5E5C2DF9" Ref="R4"  Part="1" 
AR Path="/5E28CC93/5E5C2DF9" Ref="R?"  Part="1" 
F 0 "R4" V 2500 5200 50  0000 C CNN
F 1 "?" V 2500 5200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2500 5200 50  0001 C CNN
F 3 "" H 2500 5200 50  0001 C CNN
	1    2500 5200
	0    -1   -1   0   
$EndComp
Connection ~ 2350 5200
$Comp
L Device:R R2
U 1 1 5E5C2E00
P 2200 5200
AR Path="/5E5C2E00" Ref="R2"  Part="1" 
AR Path="/5E28CC93/5E5C2E00" Ref="R?"  Part="1" 
F 0 "R2" V 2200 5200 50  0000 C CNN
F 1 "?" V 2200 5200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2200 5200 50  0001 C CNN
F 3 "" H 2200 5200 50  0001 C CNN
	1    2200 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP_Small C9
U 1 1 5E5C2E06
P 2200 6000
AR Path="/5E5C2E06" Ref="C9"  Part="1" 
AR Path="/5E28CC93/5E5C2E06" Ref="C?"  Part="1" 
F 0 "C9" H 2292 6047 50  0000 L CNN
F 1 "4.7µF" H 2292 5954 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2200 6000 50  0001 C CNN
F 3 "~" H 2200 6000 50  0001 C CNN
	1    2200 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6100 2650 6100
Wire Wire Line
	2650 5900 2200 5900
$Comp
L Device:C_Small C15
U 1 1 5E5C2E0E
P 2650 6000
AR Path="/5E5C2E0E" Ref="C15"  Part="1" 
AR Path="/5E28CC93/5E5C2E0E" Ref="C?"  Part="1" 
F 0 "C15" H 2742 6047 50  0000 L CNN
F 1 "100nF" H 2742 5954 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 6000 50  0001 C CNN
F 3 "~" H 2650 6000 50  0001 C CNN
	1    2650 6000
	1    0    0    -1  
$EndComp
Connection ~ 2200 6100
NoConn ~ 4100 5700
$Comp
L power:GNDD #PWR012
U 1 1 5E5C2E16
P 1950 4800
AR Path="/5E5C2E16" Ref="#PWR012"  Part="1" 
AR Path="/5E28CC93/5E5C2E16" Ref="#PWR?"  Part="1" 
F 0 "#PWR012" H 1950 4550 50  0001 C CNN
F 1 "GNDD" H 1955 4644 50  0000 C CNN
F 2 "" H 1950 4800 50  0001 C CNN
F 3 "" H 1950 4800 50  0001 C CNN
	1    1950 4800
	1    0    0    -1  
$EndComp
Connection ~ 2200 4800
Wire Wire Line
	1950 4800 2200 4800
$Comp
L Device:CP_Small C8
U 1 1 5E5C2E1E
P 2200 4900
AR Path="/5E5C2E1E" Ref="C8"  Part="1" 
AR Path="/5E28CC93/5E5C2E1E" Ref="C?"  Part="1" 
F 0 "C8" H 2000 4850 50  0000 L CNN
F 1 "4.7µF" H 1900 4950 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2200 4900 50  0001 C CNN
F 3 "~" H 2200 4900 50  0001 C CNN
	1    2200 4900
	-1   0    0    1   
$EndComp
$Comp
L Device:CP_Small C7
U 1 1 5E5C2E24
P 2200 4700
AR Path="/5E5C2E24" Ref="C7"  Part="1" 
AR Path="/5E28CC93/5E5C2E24" Ref="C?"  Part="1" 
F 0 "C7" H 2292 4747 50  0000 L CNN
F 1 "10µF" H 2292 4654 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2200 4700 50  0001 C CNN
F 3 "~" H 2200 4700 50  0001 C CNN
	1    2200 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4800 3000 4800
Wire Wire Line
	2650 4800 2200 4800
Connection ~ 2650 4800
Wire Wire Line
	2650 4600 3100 4600
Wire Wire Line
	2650 4600 2200 4600
Connection ~ 2650 4600
$Comp
L Device:C_Small C13
U 1 1 5E5C2E30
P 2650 4700
AR Path="/5E5C2E30" Ref="C13"  Part="1" 
AR Path="/5E28CC93/5E5C2E30" Ref="C?"  Part="1" 
F 0 "C13" H 2742 4747 50  0000 L CNN
F 1 "100nF" H 2742 4654 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 4700 50  0001 C CNN
F 3 "~" H 2650 4700 50  0001 C CNN
	1    2650 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 5000 2900 5000
Wire Wire Line
	2200 5000 2650 5000
Connection ~ 2650 5000
$Comp
L Device:C_Small C14
U 1 1 5E5C2E39
P 2650 4900
AR Path="/5E5C2E39" Ref="C14"  Part="1" 
AR Path="/5E28CC93/5E5C2E39" Ref="C?"  Part="1" 
F 0 "C14" H 2742 4947 50  0000 L CNN
F 1 "100nF" H 2742 4854 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 4900 50  0001 C CNN
F 3 "~" H 2650 4900 50  0001 C CNN
	1    2650 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR05
U 1 1 5E5C2E66
P 850 1250
AR Path="/5E5C2E66" Ref="#PWR05"  Part="1" 
AR Path="/5E28CC93/5E5C2E66" Ref="#PWR?"  Part="1" 
F 0 "#PWR05" H 850 1000 50  0001 C CNN
F 1 "GNDD" H 855 1094 50  0000 C CNN
F 2 "" H 850 1250 50  0001 C CNN
F 3 "" H 850 1250 50  0001 C CNN
	1    850  1250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J2
U 1 1 5E5C2E72
P 700 2200
AR Path="/5E5C2E72" Ref="J2"  Part="1" 
AR Path="/5E28CC93/5E5C2E72" Ref="J?"  Part="1" 
F 0 "J2" H 700 1700 50  0000 C CNN
F 1 "Screw Terminal 01x03" H 350 1800 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 700 2200 50  0001 C CNN
F 3 "~" H 700 2200 50  0001 C CNN
	1    700  2200
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR08
U 1 1 5E5C2E78
P 1500 2200
AR Path="/5E5C2E78" Ref="#PWR08"  Part="1" 
AR Path="/5E28CC93/5E5C2E78" Ref="#PWR?"  Part="1" 
F 0 "#PWR08" H 1500 2050 50  0001 C CNN
F 1 "+3.3V" H 1515 2373 50  0000 C CNN
F 2 "" H 1500 2200 50  0001 C CNN
F 3 "" H 1500 2200 50  0001 C CNN
	1    1500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  2200 1500 2200
Text Label 4100 5900 0    50   ~ 0
ADC_IRQ
Text Label 4100 3450 0    50   ~ 0
ADC1_CS
Wire Wire Line
	2650 2750 3100 2750
Wire Wire Line
	2650 2750 2200 2750
Connection ~ 2650 2750
$Comp
L Device:C_Small C10
U 1 1 5E5C2E9E
P 2650 2850
AR Path="/5E5C2E9E" Ref="C10"  Part="1" 
AR Path="/5E28CC93/5E5C2E9E" Ref="C?"  Part="1" 
F 0 "C10" H 2742 2897 50  0000 L CNN
F 1 "100nF" H 2742 2804 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 2850 50  0001 C CNN
F 3 "~" H 2650 2850 50  0001 C CNN
	1    2650 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  6850 1250 6850
Wire Wire Line
	1800 3350 2050 3350
$Comp
L Connector:Screw_Terminal_01x05 J1
U 1 1 5E5C2EAA
P 650 1050
AR Path="/5E5C2EAA" Ref="J1"  Part="1" 
AR Path="/5E28CC93/5E5C2EAA" Ref="J?"  Part="1" 
F 0 "J1" H 650 600 50  0000 C CNN
F 1 "Screw Terminal 01x06" H 300 700 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-5_P5.08mm" H 650 1050 50  0001 C CNN
F 3 "~" H 650 1050 50  0001 C CNN
	1    650  1050
	-1   0    0    1   
$EndComp
Wire Wire Line
	2050 5200 1800 5200
Text Label 4100 5300 0    50   ~ 0
ADC2_CS
Connection ~ 3100 5400
Wire Wire Line
	1150 3650 700  3650
Wire Wire Line
	700  3650 700  4050
Wire Wire Line
	800  4050 700  4050
Connection ~ 700  4050
Wire Wire Line
	700  4050 700  4250
Wire Wire Line
	1150 5900 1000 5900
Wire Notes Line
	3500 2550 3500 550 
Text Notes 3450 2500 2    100  ~ 20
Connectors
Wire Notes Line
	550  2550 5550 2550
Text Notes 5500 6450 2    100  ~ 20
Measurements
Text Notes 5500 2500 2    100  ~ 20
Radio
Wire Notes Line
	5550 550  5550 6500
Wire Notes Line
	550  550  5550 550 
Wire Notes Line
	550  550  550  7600
Text Notes 3450 7550 2    100  ~ 20
Power Supply
$Comp
L power:+3.3V #PWR09
U 1 1 5E5C2F69
P 1850 800
AR Path="/5E5C2F69" Ref="#PWR09"  Part="1" 
AR Path="/5E28CC93/5E5C2F69" Ref="#PWR?"  Part="1" 
F 0 "#PWR09" H 1850 650 50  0001 C CNN
F 1 "+3.3V" H 1865 973 50  0000 C CNN
F 2 "" H 1850 800 50  0001 C CNN
F 3 "" H 1850 800 50  0001 C CNN
	1    1850 800 
	1    0    0    -1  
$EndComp
Text Label 2350 1400 0    50   ~ 0
RESET
Text Label 2350 1300 0    50   ~ 0
SCK
Text Label 2350 1200 0    50   ~ 0
MOSI
Text Label 2350 1100 0    50   ~ 0
MISO
$Comp
L power:GND #PWR010
U 1 1 5E5C2F73
P 1850 1700
AR Path="/5E5C2F73" Ref="#PWR010"  Part="1" 
AR Path="/5E28CC93/5E5C2F73" Ref="#PWR?"  Part="1" 
F 0 "#PWR010" H 1850 1450 50  0001 C CNN
F 1 "GND" H 1855 1527 50  0000 C CNN
F 2 "" H 1850 1700 50  0001 C CNN
F 3 "" H 1850 1700 50  0001 C CNN
	1    1850 1700
	1    0    0    -1  
$EndComp
$Comp
L main-rescue:AVR-ISP-6-Connector J3
U 1 1 5E5C2F79
P 1950 1300
AR Path="/5E5C2F79" Ref="J3"  Part="1" 
AR Path="/5E28CC93/5E5C2F79" Ref="J?"  Part="1" 
F 0 "J3" H 2100 1800 50  0000 R CNN
F 1 "AVR-ISP-6" H 2300 1700 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" V 1700 1350 50  0001 C CNN
F 3 " ~" H 675 750 50  0001 C CNN
	1    1950 1300
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR06
U 1 1 5E5C2F7F
P 1250 2100
AR Path="/5E5C2F7F" Ref="#PWR06"  Part="1" 
AR Path="/5E28CC93/5E5C2F7F" Ref="#PWR?"  Part="1" 
F 0 "#PWR06" H 1250 1950 50  0001 C CNN
F 1 "+BATT" H 1265 2273 50  0000 C CNN
F 2 "" H 1250 2100 50  0001 C CNN
F 3 "" H 1250 2100 50  0001 C CNN
	1    1250 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR03
U 1 1 5E5C2F85
P 700 6850
AR Path="/5E5C2F85" Ref="#PWR03"  Part="1" 
AR Path="/5E28CC93/5E5C2F85" Ref="#PWR?"  Part="1" 
F 0 "#PWR03" H 700 6700 50  0001 C CNN
F 1 "+BATT" H 715 7023 50  0000 C CNN
F 2 "" H 700 6850 50  0001 C CNN
F 3 "" H 700 6850 50  0001 C CNN
	1    700  6850
	1    0    0    -1  
$EndComp
Connection ~ 700  6850
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5E735F29
P 2850 1200
F 0 "J4" H 2959 1487 50  0000 C CNN
F 1 "Conn_01x04_Male" V 2800 1250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2850 1200 50  0001 C CNN
F 3 "~" H 2850 1200 50  0001 C CNN
	1    2850 1200
	1    0    0    -1  
$EndComp
Text Label 3050 1200 0    50   ~ 0
RXD
Text Label 3050 1300 0    50   ~ 0
TXD
$Comp
L power:GND #PWR0101
U 1 1 5E764D88
P 3050 1400
AR Path="/5E764D88" Ref="#PWR0101"  Part="1" 
AR Path="/5E28CC93/5E764D88" Ref="#PWR?"  Part="1" 
F 0 "#PWR0101" H 3050 1150 50  0001 C CNN
F 1 "GND" H 3055 1227 50  0000 C CNN
F 2 "" H 3050 1400 50  0001 C CNN
F 3 "" H 3050 1400 50  0001 C CNN
	1    3050 1400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5E765E11
P 3050 1100
AR Path="/5E765E11" Ref="#PWR0102"  Part="1" 
AR Path="/5E28CC93/5E765E11" Ref="#PWR?"  Part="1" 
F 0 "#PWR0102" H 3050 950 50  0001 C CNN
F 1 "+3.3V" H 3065 1273 50  0000 C CNN
F 2 "" H 3050 1100 50  0001 C CNN
F 3 "" H 3050 1100 50  0001 C CNN
	1    3050 1100
	1    0    0    -1  
$EndComp
Connection ~ 1150 3850
Text Label 1150 6200 0    50   ~ 0
MOTOR_SHUNT
Text Label 850  1150 0    50   ~ 0
MOTOR_SHUNT
Text Label 850  950  0    50   ~ 0
BATTERY_SENSOR
Text Label 1800 3350 2    50   ~ 0
BATTERY_SENSOR
Text Label 1150 4350 0    50   ~ 0
ARRAY_SHUNT
Text Label 850  1050 0    50   ~ 0
ARRAY_SHUNT
Text Label 850  850  0    50   ~ 0
MISCELLANEOUS
Text Label 1800 5200 2    50   ~ 0
MISCELLANEOUS
$Comp
L power:GNDPWR #PWR0105
U 1 1 5E8C95FB
P 900 2300
F 0 "#PWR0105" H 900 2100 50  0001 C CNN
F 1 "GNDPWR" H 906 2145 50  0000 C CNN
F 2 "" H 900 2250 50  0001 C CNN
F 3 "" H 900 2250 50  0001 C CNN
	1    900  2300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E8B2BD2
P 900 2100
F 0 "#FLG0101" H 900 2175 50  0001 C CNN
F 1 "PWR_FLAG" H 900 2277 50  0000 C CNN
F 2 "" H 900 2100 50  0001 C CNN
F 3 "~" H 900 2100 50  0001 C CNN
	1    900  2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5E5C2E6C
P 1250 2300
AR Path="/5E5C2E6C" Ref="#PWR07"  Part="1" 
AR Path="/5E28CC93/5E5C2E6C" Ref="#PWR?"  Part="1" 
F 0 "#PWR07" H 1250 2050 50  0001 C CNN
F 1 "GND" H 1255 2127 50  0000 C CNN
F 2 "" H 1250 2300 50  0001 C CNN
F 3 "" H 1250 2300 50  0001 C CNN
	1    1250 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  2300 1250 2300
Connection ~ 900  2300
Wire Wire Line
	900  2100 1250 2100
Connection ~ 900  2100
Connection ~ 3100 3550
$Comp
L main:ADE7912 U2
U 1 1 5E5C2C23
P 3600 3700
AR Path="/5E5C2C23" Ref="U2"  Part="1" 
AR Path="/5E28CC93/5E5C2C23" Ref="U?"  Part="1" 
F 0 "U2" H 3600 4465 50  0000 C CNN
F 1 "ADE7912" H 3600 4374 50  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 3550 2950 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADE7912_7913.pdf" H 3550 2950 50  0001 C CNN
	1    3600 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR015
U 1 1 5E5C2D0D
P 2650 3400
AR Path="/5E5C2D0D" Ref="#PWR015"  Part="1" 
AR Path="/5E28CC93/5E5C2D0D" Ref="#PWR?"  Part="1" 
F 0 "#PWR015" H 2650 3150 50  0001 C CNN
F 1 "GNDD" H 2650 3250 50  0000 C CNN
F 2 "" H 2650 3400 50  0001 C CNN
F 3 "" H 2650 3400 50  0001 C CNN
	1    2650 3400
	1    0    0    -1  
$EndComp
Connection ~ 700  5900
Wire Wire Line
	700  5900 700  6100
Wire Wire Line
	700  5500 700  5900
Wire Wire Line
	1150 5700 1150 5500
Wire Wire Line
	800  5900 700  5900
Wire Wire Line
	1150 5500 700  5500
$Comp
L power:GNDD #PWR02
U 1 1 5E5C2F18
P 700 6100
AR Path="/5E5C2F18" Ref="#PWR02"  Part="1" 
AR Path="/5E28CC93/5E5C2F18" Ref="#PWR?"  Part="1" 
F 0 "#PWR02" H 700 5850 50  0001 C CNN
F 1 "GNDD" H 704 5945 50  0000 C CNN
F 2 "" H 700 6100 50  0001 C CNN
F 3 "" H 700 6100 50  0001 C CNN
	1    700  6100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5E5C2F11
P 900 5900
AR Path="/5E5C2F11" Ref="C3"  Part="1" 
AR Path="/5E28CC93/5E5C2F11" Ref="C?"  Part="1" 
F 0 "C3" V 668 5900 50  0000 C CNN
F 1 "33nF" V 761 5900 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 900 5900 50  0001 C CNN
F 3 "~" H 900 5900 50  0001 C CNN
	1    900  5900
	0    1    1    0   
$EndComp
Text Label 8300 5750 0    50   ~ 0
LOWPOWERMODE_SW
Text Label 8300 5650 0    50   ~ 0
ENABLE_SW
Text Label 8300 5350 0    50   ~ 0
DISPLAY_CS
Text Label 8300 5550 0    50   ~ 0
MOSI
Text Label 8300 5450 0    50   ~ 0
SCK
$Sheet
S 7400 5250 900  600 
U 5E5C2F57
F0 "User Interface" 50
F1 "user_interface.sch" 50
F2 "MOSI" I R 8300 5550 50 
F3 "DISPLAY_CS" I R 8300 5350 50 
F4 "SCK" I R 8300 5450 50 
F5 "ENABLE_SW" I R 8300 5650 50 
F6 "LOWPOWERMODE_SW" I R 8300 5750 50 
$EndSheet
Wire Notes Line
	3500 7600 550  7600
Wire Notes Line
	550  6500 5550 6500
$Comp
L power:GND #PWR032
U 1 1 5E5C2E48
P 7400 4100
AR Path="/5E5C2E48" Ref="#PWR032"  Part="1" 
AR Path="/5E28CC93/5E5C2E48" Ref="#PWR?"  Part="1" 
F 0 "#PWR032" H 7400 3850 50  0001 C CNN
F 1 "GND" H 7405 3927 50  0000 C CNN
F 2 "" H 7400 4100 50  0001 C CNN
F 3 "" H 7400 4100 50  0001 C CNN
	1    7400 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5E5C2E7F
P 8550 4100
AR Path="/5E5C2E7F" Ref="#PWR034"  Part="1" 
AR Path="/5E28CC93/5E5C2E7F" Ref="#PWR?"  Part="1" 
F 0 "#PWR034" H 8550 3850 50  0001 C CNN
F 1 "GND" H 8555 3927 50  0000 C CNN
F 2 "" H 8550 4100 50  0001 C CNN
F 3 "" H 8550 4100 50  0001 C CNN
	1    8550 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR035
U 1 1 5E5C2EDC
P 8850 4100
AR Path="/5E5C2EDC" Ref="#PWR035"  Part="1" 
AR Path="/5E28CC93/5E5C2EDC" Ref="#PWR?"  Part="1" 
F 0 "#PWR035" H 8850 3850 50  0001 C CNN
F 1 "GND" H 8855 3927 50  0000 C CNN
F 2 "" H 8850 4100 50  0001 C CNN
F 3 "" H 8850 4100 50  0001 C CNN
	1    8850 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR031
U 1 1 5E5C2C3B
P 7400 950
AR Path="/5E5C2C3B" Ref="#PWR031"  Part="1" 
AR Path="/5E28CC93/5E5C2C3B" Ref="#PWR?"  Part="1" 
F 0 "#PWR031" H 7400 800 50  0001 C CNN
F 1 "+3.3V" H 7415 1123 50  0000 C CNN
F 2 "" H 7400 950 50  0001 C CNN
F 3 "" H 7400 950 50  0001 C CNN
	1    7400 950 
	1    0    0    -1  
$EndComp
Text Label 8000 2900 0    50   ~ 0
RESET
$Comp
L Device:R_Small R5
U 1 1 5E5C2C80
P 6450 1900
AR Path="/5E5C2C80" Ref="R5"  Part="1" 
AR Path="/5E28CC93/5E5C2C80" Ref="R?"  Part="1" 
F 0 "R5" H 6509 1946 50  0000 L CNN
F 1 "15K" H 6509 1855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 6450 1900 50  0001 C CNN
F 3 "~" H 6450 1900 50  0001 C CNN
	1    6450 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5E5C2C86
P 6450 2200
AR Path="/5E5C2C86" Ref="R6"  Part="1" 
AR Path="/5E28CC93/5E5C2C86" Ref="R?"  Part="1" 
F 0 "R6" H 6391 2154 50  0000 R CNN
F 1 "1K" H 6391 2245 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 6450 2200 50  0001 C CNN
F 3 "~" H 6450 2200 50  0001 C CNN
	1    6450 2200
	-1   0    0    1   
$EndComp
$Comp
L power:+BATT #PWR029
U 1 1 5E5C2C8C
P 6450 1800
AR Path="/5E5C2C8C" Ref="#PWR029"  Part="1" 
AR Path="/5E28CC93/5E5C2C8C" Ref="#PWR?"  Part="1" 
F 0 "#PWR029" H 6450 1650 50  0001 C CNN
F 1 "+BATT" H 6465 1973 50  0000 C CNN
F 2 "" H 6450 1800 50  0001 C CNN
F 3 "" H 6450 1800 50  0001 C CNN
	1    6450 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR030
U 1 1 5E5C2C92
P 6450 2300
AR Path="/5E5C2C92" Ref="#PWR030"  Part="1" 
AR Path="/5E28CC93/5E5C2C92" Ref="#PWR?"  Part="1" 
F 0 "#PWR030" H 6450 2050 50  0001 C CNN
F 1 "GND" H 6455 2127 50  0000 C CNN
F 2 "" H 6450 2300 50  0001 C CNN
F 3 "" H 6450 2300 50  0001 C CNN
	1    6450 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 2000 8250 1950
Wire Wire Line
	8250 1950 8500 1950
Wire Wire Line
	8250 2100 8250 2150
Wire Wire Line
	8250 2150 8500 2150
$Comp
L Device:C_Small C24
U 1 1 5E5C2D28
P 9100 1850
AR Path="/5E5C2D28" Ref="C24"  Part="1" 
AR Path="/5E28CC93/5E5C2D28" Ref="C?"  Part="1" 
F 0 "C24" V 9000 1850 50  0000 C CNN
F 1 "22pF" V 9050 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9100 1850 50  0001 C CNN
F 3 "~" H 9100 1850 50  0001 C CNN
	1    9100 1850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C25
U 1 1 5E5C2D2E
P 9100 2250
AR Path="/5E5C2D2E" Ref="C25"  Part="1" 
AR Path="/5E28CC93/5E5C2D2E" Ref="C?"  Part="1" 
F 0 "C25" V 9329 2250 50  0000 C CNN
F 1 "22pF" V 9238 2250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9100 2250 50  0001 C CNN
F 3 "~" H 9100 2250 50  0001 C CNN
	1    9100 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8500 1950 8500 1850
Wire Wire Line
	8500 1850 9000 1850
Wire Wire Line
	8500 2150 8500 2250
Wire Wire Line
	8500 2250 9000 2250
Wire Wire Line
	9200 1850 9200 2050
Wire Wire Line
	9200 2050 9450 2050
Connection ~ 9200 2050
Wire Wire Line
	9200 2250 9200 2050
Text Label 8000 2000 0    50   ~ 0
PB6
Text Label 8000 2100 0    50   ~ 0
PB7
Text Label 8000 2700 0    50   ~ 0
ENABLE_SW
Text Label 8000 2800 0    50   ~ 0
DISPLAY_CS
Text Label 8000 3100 0    50   ~ 0
RXD
Text Label 8000 3200 0    50   ~ 0
TXD
Text Label 6800 1400 2    50   ~ 0
AREF
$Comp
L power:GND #PWR033
U 1 1 5E5C2D68
P 7850 950
AR Path="/5E5C2D68" Ref="#PWR033"  Part="1" 
AR Path="/5E28CC93/5E5C2D68" Ref="#PWR?"  Part="1" 
F 0 "#PWR033" H 7850 700 50  0001 C CNN
F 1 "GND" H 7855 777 50  0000 C CNN
F 2 "" H 7850 950 50  0001 C CNN
F 3 "" H 7850 950 50  0001 C CNN
	1    7850 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C23
U 1 1 5E5C2D70
P 7750 950
AR Path="/5E5C2D70" Ref="C23"  Part="1" 
AR Path="/5E28CC93/5E5C2D70" Ref="C?"  Part="1" 
F 0 "C23" V 7521 950 50  0000 C CNN
F 1 "100nF" V 7612 950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7750 950 50  0001 C CNN
F 3 "~" H 7750 950 50  0001 C CNN
	1    7750 950 
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR028
U 1 1 5E5C2E3F
P 6150 1600
AR Path="/5E5C2E3F" Ref="#PWR028"  Part="1" 
AR Path="/5E28CC93/5E5C2E3F" Ref="#PWR?"  Part="1" 
F 0 "#PWR028" H 6150 1350 50  0001 C CNN
F 1 "GND" H 6155 1427 50  0000 C CNN
F 2 "" H 6150 1600 50  0001 C CNN
F 3 "" H 6150 1600 50  0001 C CNN
	1    6150 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 2000 8250 2000
Connection ~ 7400 950 
Wire Wire Line
	7400 1100 7400 950 
$Comp
L Device:C_Small C22
U 1 1 5E5C2E4E
P 6150 1500
AR Path="/5E5C2E4E" Ref="C22"  Part="1" 
AR Path="/5E28CC93/5E5C2E4E" Ref="C?"  Part="1" 
F 0 "C22" H 5950 1550 50  0000 C CNN
F 1 "100nF" H 6000 1450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6150 1500 50  0001 C CNN
F 3 "~" H 6150 1500 50  0001 C CNN
	1    6150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 950  7500 950 
Wire Wire Line
	7650 950  7500 950 
Connection ~ 7500 950 
Wire Wire Line
	7500 950  7500 1100
Wire Wire Line
	8000 2100 8250 2100
Text Label 8000 3300 0    50   ~ 0
RADIO_IRQ
$Comp
L main-rescue:ATmega328P-AU-MCU_Microchip_ATmega U5
U 1 1 5E5C2E59
P 7400 2600
AR Path="/5E5C2E59" Ref="U5"  Part="1" 
AR Path="/5E28CC93/5E5C2E59" Ref="U?"  Part="1" 
F 0 "U5" H 6759 2646 50  0000 R CNN
F 1 "ATmega328P-AU" H 6759 2555 50  0000 R CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 7400 2600 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 7400 2600 50  0001 C CNN
	1    7400 2600
	1    0    0    -1  
$EndComp
Text Label 8000 1900 0    50   ~ 0
SCK
Text Label 8000 1700 0    50   ~ 0
MOSI
Text Label 8000 1800 0    50   ~ 0
MISO
Text Label 8000 1600 0    50   ~ 0
RADIO_CS
Text Label 8000 3400 0    50   ~ 0
ADC_IRQ
$Comp
L Device:R R8
U 1 1 5E5C2E85
P 8400 3800
AR Path="/5E5C2E85" Ref="R8"  Part="1" 
AR Path="/5E28CC93/5E5C2E85" Ref="R?"  Part="1" 
F 0 "R8" V 8500 3650 50  0000 C CNN
F 1 "1K" V 8400 3800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8330 3800 50  0001 C CNN
F 3 "~" H 8400 3800 50  0001 C CNN
	1    8400 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	8250 3800 8000 3800
$Comp
L Device:R R10
U 1 1 5E5C2E8C
P 8950 2900
AR Path="/5E5C2E8C" Ref="R10"  Part="1" 
AR Path="/5E28CC93/5E5C2E8C" Ref="R?"  Part="1" 
F 0 "R10" V 8850 2850 50  0000 L CNN
F 1 "10K" V 8950 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8880 2900 50  0001 C CNN
F 3 "~" H 8950 2900 50  0001 C CNN
	1    8950 2900
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR036
U 1 1 5E5C2E93
P 9100 2900
AR Path="/5E5C2E93" Ref="#PWR036"  Part="1" 
AR Path="/5E28CC93/5E5C2E93" Ref="#PWR?"  Part="1" 
F 0 "#PWR036" H 9100 2750 50  0001 C CNN
F 1 "+3.3V" H 9115 3073 50  0000 C CNN
F 2 "" H 9100 2900 50  0001 C CNN
F 3 "" H 9100 2900 50  0001 C CNN
	1    9100 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5E5C2EB5
P 8950 3300
AR Path="/5E5C2EB5" Ref="R11"  Part="1" 
AR Path="/5E28CC93/5E5C2EB5" Ref="R?"  Part="1" 
F 0 "R11" V 8850 3300 50  0000 C CNN
F 1 "10K" V 8950 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8880 3300 50  0001 C CNN
F 3 "~" H 8950 3300 50  0001 C CNN
	1    8950 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5E5C2EBB
P 8950 3400
AR Path="/5E5C2EBB" Ref="R12"  Part="1" 
AR Path="/5E28CC93/5E5C2EBB" Ref="R?"  Part="1" 
F 0 "R12" V 9050 3400 50  0000 C CNN
F 1 "10K" V 8950 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8880 3400 50  0001 C CNN
F 3 "~" H 8950 3400 50  0001 C CNN
	1    8950 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	9100 3300 9100 3400
Connection ~ 9100 3400
Wire Wire Line
	9100 3500 9100 3400
Wire Wire Line
	8000 3400 8800 3400
Wire Wire Line
	8000 3300 8800 3300
$Comp
L power:GND #PWR038
U 1 1 5E5C2EC3
P 9100 3500
AR Path="/5E5C2EC3" Ref="#PWR038"  Part="1" 
AR Path="/5E28CC93/5E5C2EC3" Ref="#PWR?"  Part="1" 
F 0 "#PWR038" H 9100 3250 50  0001 C CNN
F 1 "GND" H 9200 3400 50  0000 C CNN
F 2 "" H 9100 3500 50  0001 C CNN
F 3 "" H 9100 3500 50  0001 C CNN
	1    9100 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5E5C2ECA
P 8400 3700
AR Path="/5E5C2ECA" Ref="R7"  Part="1" 
AR Path="/5E28CC93/5E5C2ECA" Ref="R?"  Part="1" 
F 0 "R7" V 8300 3700 50  0000 C CNN
F 1 "1K" V 8400 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8330 3700 50  0001 C CNN
F 3 "~" H 8400 3700 50  0001 C CNN
	1    8400 3700
	0    1    1    0   
$EndComp
$Comp
L Device:LED D6
U 1 1 5E5C2ED0
P 8550 3950
AR Path="/5E5C2ED0" Ref="D6"  Part="1" 
AR Path="/5E28CC93/5E5C2ED0" Ref="D?"  Part="1" 
F 0 "D6" V 8500 4100 50  0000 R CNN
F 1 "LED" V 8400 4150 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8550 3950 50  0001 C CNN
F 3 "~" H 8550 3950 50  0001 C CNN
	1    8550 3950
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 5E5C2ED6
P 8850 3950
AR Path="/5E5C2ED6" Ref="D7"  Part="1" 
AR Path="/5E28CC93/5E5C2ED6" Ref="D?"  Part="1" 
F 0 "D7" V 8890 3829 50  0000 R CNN
F 1 "LED" V 8797 3829 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8850 3950 50  0001 C CNN
F 3 "~" H 8850 3950 50  0001 C CNN
	1    8850 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8000 3700 8250 3700
Wire Wire Line
	8550 3700 8850 3700
Wire Wire Line
	8850 3700 8850 3800
Text Label 8000 1500 0    50   ~ 0
ADC1_CS
Text Label 8000 1400 0    50   ~ 0
ADC2_CS
$Comp
L Device:R R?
U 1 1 5E5C2EEA
P 8700 1250
AR Path="/5E28CC93/5E5C2EEA" Ref="R?"  Part="1" 
AR Path="/5E5C2EEA" Ref="R9"  Part="1" 
F 0 "R9" H 8770 1297 50  0000 L CNN
F 1 "10K" H 8770 1204 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8630 1250 50  0001 C CNN
F 3 "~" H 8700 1250 50  0001 C CNN
	1    8700 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E5C2EF0
P 9000 1250
AR Path="/5E28CC93/5E5C2EF0" Ref="R?"  Part="1" 
AR Path="/5E5C2EF0" Ref="R13"  Part="1" 
F 0 "R13" H 9070 1297 50  0000 L CNN
F 1 "10K" H 9070 1204 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8930 1250 50  0001 C CNN
F 3 "~" H 9000 1250 50  0001 C CNN
	1    9000 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E5C2EF6
P 9300 1250
AR Path="/5E28CC93/5E5C2EF6" Ref="R?"  Part="1" 
AR Path="/5E5C2EF6" Ref="R14"  Part="1" 
F 0 "R14" H 9370 1297 50  0000 L CNN
F 1 "10K" H 9370 1204 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9230 1250 50  0001 C CNN
F 3 "~" H 9300 1250 50  0001 C CNN
	1    9300 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 1400 8700 1400
Wire Wire Line
	8000 1500 9000 1500
Wire Wire Line
	9000 1500 9000 1400
Wire Wire Line
	8000 1600 9300 1600
Wire Wire Line
	9300 1600 9300 1400
$Comp
L power:+3.3V #PWR037
U 1 1 5E5C2F01
P 9000 1100
AR Path="/5E5C2F01" Ref="#PWR037"  Part="1" 
AR Path="/5E28CC93/5E5C2F01" Ref="#PWR?"  Part="1" 
F 0 "#PWR037" H 9000 950 50  0001 C CNN
F 1 "+3.3V" H 9015 1273 50  0000 C CNN
F 2 "" H 9000 1100 50  0001 C CNN
F 3 "" H 9000 1100 50  0001 C CNN
	1    9000 1100
	1    0    0    -1  
$EndComp
Connection ~ 9000 1100
Wire Wire Line
	8700 1100 9000 1100
Wire Wire Line
	9000 1100 9300 1100
Text Label 8000 2600 0    50   ~ 0
LOWPOWERMODE_SW
Wire Wire Line
	6150 1400 6800 1400
Wire Wire Line
	6800 1600 6700 1600
Wire Wire Line
	6700 1600 6700 2050
Wire Wire Line
	6450 2000 6450 2050
Wire Wire Line
	6450 2050 6450 2100
Connection ~ 6450 2050
Wire Wire Line
	6700 2050 6450 2050
Wire Wire Line
	8000 2900 8800 2900
NoConn ~ 6800 1700
NoConn ~ 8000 2500
NoConn ~ 8000 2400
NoConn ~ 8000 2300
NoConn ~ 8000 3600
NoConn ~ 8000 3500
Connection ~ 8500 2150
Connection ~ 8500 1950
$Comp
L Device:Crystal_Small Y2
U 1 1 5E5C2D17
P 8500 2050
AR Path="/5E5C2D17" Ref="Y2"  Part="1" 
AR Path="/5E28CC93/5E5C2D17" Ref="Y?"  Part="1" 
F 0 "Y2" V 8500 2000 50  0000 L CNN
F 1 "16MHz" V 8545 2138 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-U_Vertical" H 8500 2050 50  0001 C CNN
F 3 "~" H 8500 2050 50  0001 C CNN
	1    8500 2050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR039
U 1 1 5E5C2D22
P 9450 2050
AR Path="/5E5C2D22" Ref="#PWR039"  Part="1" 
AR Path="/5E28CC93/5E5C2D22" Ref="#PWR?"  Part="1" 
F 0 "#PWR039" H 9450 1800 50  0001 C CNN
F 1 "GND" H 9455 1877 50  0000 C CNN
F 2 "" H 9450 2050 50  0001 C CNN
F 3 "" H 9450 2050 50  0001 C CNN
	1    9450 2050
	1    0    0    -1  
$EndComp
NoConn ~ 5000 1250
Text Label 5000 1950 0    50   ~ 0
RADIO_IRQ
$Comp
L power:GND #PWR021
U 1 1 5E5C2F4B
P 4500 2150
AR Path="/5E5C2F4B" Ref="#PWR021"  Part="1" 
AR Path="/5E28CC93/5E5C2F4B" Ref="#PWR?"  Part="1" 
F 0 "#PWR021" H 4500 1900 50  0001 C CNN
F 1 "GND" H 4505 1977 50  0000 C CNN
F 2 "" H 4500 2150 50  0001 C CNN
F 3 "" H 4500 2150 50  0001 C CNN
	1    4500 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2150 4500 2150
Connection ~ 4500 2150
Wire Wire Line
	4500 2150 4600 2150
$Comp
L power:+3.3V #PWR020
U 1 1 5E5C2F42
P 4500 900
AR Path="/5E5C2F42" Ref="#PWR020"  Part="1" 
AR Path="/5E28CC93/5E5C2F42" Ref="#PWR?"  Part="1" 
F 0 "#PWR020" H 4500 750 50  0001 C CNN
F 1 "+3.3V" H 4515 1073 50  0000 C CNN
F 2 "" H 4500 900 50  0001 C CNN
F 3 "" H 4500 900 50  0001 C CNN
	1    4500 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5E5C2F3C
P 4850 900
AR Path="/5E5C2F3C" Ref="#PWR024"  Part="1" 
AR Path="/5E28CC93/5E5C2F3C" Ref="#PWR?"  Part="1" 
F 0 "#PWR024" H 4850 650 50  0001 C CNN
F 1 "GND" H 4855 727 50  0000 C CNN
F 2 "" H 4850 900 50  0001 C CNN
F 3 "" H 4850 900 50  0001 C CNN
	1    4850 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C17
U 1 1 5E5C2F36
P 4750 900
AR Path="/5E5C2F36" Ref="C17"  Part="1" 
AR Path="/5E28CC93/5E5C2F36" Ref="C?"  Part="1" 
F 0 "C17" V 4521 900 50  0000 C CNN
F 1 "100nF" V 4612 900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4750 900 50  0001 C CNN
F 3 "~" H 4750 900 50  0001 C CNN
	1    4750 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 900  4500 900 
Text Label 4000 1450 2    50   ~ 0
MISO
Text Label 4000 1350 2    50   ~ 0
MOSI
Text Label 4000 1250 2    50   ~ 0
SCK
Text Label 4000 1550 2    50   ~ 0
RADIO_CS
Text Label 4000 1750 2    50   ~ 0
RESET
NoConn ~ 5000 1850
NoConn ~ 5000 1750
NoConn ~ 5000 1650
NoConn ~ 5000 1550
NoConn ~ 5000 1450
Connection ~ 4500 900 
Wire Wire Line
	4500 900  4500 1050
$Comp
L main-rescue:RFM95W-868S2-RF_Module U4
U 1 1 5E5C2F23
P 4500 1550
AR Path="/5E5C2F23" Ref="U4"  Part="1" 
AR Path="/5E28CC93/5E5C2F23" Ref="U?"  Part="1" 
F 0 "U4" H 4100 2200 50  0000 C CNN
F 1 "RFM95W-868S2" H 4000 2100 50  0000 C CNN
F 2 "footprints:RFM9x" H 1200 3200 50  0001 C CNN
F 3 "http://www.hoperf.com/upload/rf/RFM95_96_97_98W.pdf" H 1200 3200 50  0001 C CNN
	1    4500 1550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
