EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 60CE369D
P 1000 750
F 0 "H1" H 1100 796 50  0000 L CNN
F 1 "MountingHole" H 1100 705 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 1000 750 50  0001 C CNN
F 3 "~" H 1000 750 50  0001 C CNN
	1    1000 750 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 60CE48AC
P 1000 1000
F 0 "H2" H 1100 1046 50  0000 L CNN
F 1 "MountingHole" H 1100 955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 1000 1000 50  0001 C CNN
F 3 "~" H 1000 1000 50  0001 C CNN
	1    1000 1000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 60CE530F
P 1000 1250
F 0 "H3" H 1100 1296 50  0000 L CNN
F 1 "MountingHole" H 1100 1205 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 1000 1250 50  0001 C CNN
F 3 "~" H 1000 1250 50  0001 C CNN
	1    1000 1250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 60CE5E69
P 1000 1500
F 0 "H4" H 1100 1546 50  0000 L CNN
F 1 "MountingHole" H 1100 1455 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm" H 1000 1500 50  0001 C CNN
F 3 "~" H 1000 1500 50  0001 C CNN
	1    1000 1500
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:Arduino_UNO_R3 A1
U 1 1 60CC0281
P 7600 3000
F 0 "A1" H 7600 4181 50  0000 C CNN
F 1 "Arduino_UNO_R3" H 7600 4090 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 7600 3000 50  0001 C CIN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 7600 3000 50  0001 C CNN
	1    7600 3000
	1    0    0    -1  
$EndComp
$Comp
L sensor_board:20X4_LCD L?
U 1 1 60CE8289
P 5700 3500
F 0 "L?" H 6178 3551 50  0000 L CNN
F 1 "20X4_LCD" H 6178 3460 50  0000 L CNN
F 2 "" H 5700 3250 50  0001 C CNN
F 3 "" H 5700 3250 50  0001 C CNN
	1    5700 3500
	1    0    0    -1  
$EndComp
$Comp
L sensor_board:NEO-6M G?
U 1 1 60CE88E8
P 3600 3950
F 0 "G?" H 3928 4001 50  0000 L CNN
F 1 "NEO-6M" H 3928 3910 50  0000 L CNN
F 2 "" H 3600 3650 50  0001 C CNN
F 3 "" H 3600 3650 50  0001 C CNN
	1    3600 3950
	1    0    0    -1  
$EndComp
$Comp
L sensor_board:Reyax_RYLR896 R?
U 1 1 60CE8E2C
P 4250 1800
F 0 "R?" H 5878 1651 50  0000 L CNN
F 1 "Reyax_RYLR896" H 5878 1560 50  0000 L CNN
F 2 "" H 5100 1250 50  0001 C CNN
F 3 "" H 5100 1250 50  0001 C CNN
	1    4250 1800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 60CFA478
P 3000 4150
F 0 "#PWR?" H 3000 4000 50  0001 C CNN
F 1 "+3.3V" H 3015 4323 50  0000 C CNN
F 2 "" H 3000 4150 50  0001 C CNN
F 3 "" H 3000 4150 50  0001 C CNN
	1    3000 4150
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 60CFA933
P 4250 1750
F 0 "#PWR?" H 4250 1600 50  0001 C CNN
F 1 "+3.3V" H 4265 1923 50  0000 C CNN
F 2 "" H 4250 1750 50  0001 C CNN
F 3 "" H 4250 1750 50  0001 C CNN
	1    4250 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 4150 3200 4150
Wire Wire Line
	4250 1750 4550 1750
$Comp
L power:+5V #PWR?
U 1 1 60CFC4E0
P 4900 3450
F 0 "#PWR?" H 4900 3300 50  0001 C CNN
F 1 "+5V" V 4915 3578 50  0000 L CNN
F 2 "" H 4900 3450 50  0001 C CNN
F 3 "" H 4900 3450 50  0001 C CNN
	1    4900 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 3450 5100 3450
Text Label 6900 2400 0    50   ~ 0
0
Text Label 6900 2500 0    50   ~ 0
1
Text Label 4350 1950 0    50   ~ 0
1
Text Label 4350 2050 0    50   ~ 0
0
Wire Wire Line
	6900 2400 7100 2400
Wire Wire Line
	6900 2500 7100 2500
Wire Wire Line
	4350 2050 4550 2050
Wire Wire Line
	4350 1950 4550 1950
Text Label 8200 3700 0    50   ~ 0
SDA
Text Label 8200 3800 0    50   ~ 0
SCL
Text Label 4950 3550 0    50   ~ 0
SDA
Text Label 4950 3650 0    50   ~ 0
SCL
$Comp
L power:+5V #PWR?
U 1 1 60CFF0D3
P 7800 1850
F 0 "#PWR?" H 7800 1700 50  0001 C CNN
F 1 "+5V" H 7815 2023 50  0000 C CNN
F 2 "" H 7800 1850 50  0001 C CNN
F 3 "" H 7800 1850 50  0001 C CNN
	1    7800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 1850 7800 2000
Wire Wire Line
	8200 3700 8100 3700
Wire Wire Line
	8100 3800 8200 3800
Wire Wire Line
	5100 3550 4950 3550
Wire Wire Line
	5100 3650 4950 3650
Text Label 6950 3200 0    50   ~ 0
8
Text Label 6950 3300 0    50   ~ 0
9
Text Label 3100 3850 0    50   ~ 0
8
Text Label 3100 3950 0    50   ~ 0
9
Wire Wire Line
	3100 3850 3200 3850
Wire Wire Line
	3100 3950 3200 3950
Wire Wire Line
	6950 3200 7100 3200
Wire Wire Line
	6950 3300 7100 3300
$EndSCHEMATC
