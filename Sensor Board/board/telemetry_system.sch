EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 3
Title "Telemetry System"
Date "2020-01-20"
Rev ""
Comp "Staten Island Solar Car"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L main:ADE7912 U?
U 1 1 5E3239BF
P 3650 3700
AR Path="/5E3239BF" Ref="U?"  Part="1" 
AR Path="/5E28CC93/5E3239BF" Ref="U2"  Part="1" 
F 0 "U2" H 3650 4465 50  0000 C CNN
F 1 "ADE7912" H 3650 4374 50  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 3600 2950 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADE7912_7913.pdf" H 3600 2950 50  0001 C CNN
	1    3650 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E3239D1
P 4250 3150
AR Path="/5E3239D1" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E3239D1" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 4250 3000 50  0001 C CNN
F 1 "+3.3V" H 4265 3323 50  0000 C CNN
F 2 "" H 4250 3150 50  0001 C CNN
F 3 "" H 4250 3150 50  0001 C CNN
	1    4250 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E3239D7
P 4200 4200
AR Path="/5E3239D7" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E3239D7" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 4200 3950 50  0001 C CNN
F 1 "GND" H 4205 4027 50  0000 C CNN
F 2 "" H 4200 4200 50  0001 C CNN
F 3 "" H 4200 4200 50  0001 C CNN
	1    4200 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 4150 4200 4150
Wire Wire Line
	4200 4150 4200 4200
Text Label 4150 3750 0    50   ~ 0
MISO
Text Label 4150 3650 0    50   ~ 0
MOSI
Text Label 4150 3550 0    50   ~ 0
SCK
Text Label 8050 2900 0    50   ~ 0
RESET
$Comp
L power:+3.3V #PWR?
U 1 1 5E3239F3
P 7450 950
AR Path="/5E3239F3" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E3239F3" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 7450 800 50  0001 C CNN
F 1 "+3.3V" H 7465 1123 50  0000 C CNN
F 2 "" H 7450 950 50  0001 C CNN
F 3 "" H 7450 950 50  0001 C CNN
	1    7450 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5E3239F9
P 2250 4250
AR Path="/5E3239F9" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E3239F9" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 2250 4000 50  0001 C CNN
F 1 "GNDD" H 2254 4095 50  0000 C CNN
F 2 "" H 2250 4250 50  0001 C CNN
F 3 "" H 2250 4250 50  0001 C CNN
	1    2250 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal_Small Y?
U 1 1 5E3239FF
P 4700 3850
AR Path="/5E3239FF" Ref="Y?"  Part="1" 
AR Path="/5E28CC93/5E3239FF" Ref="Y1"  Part="1" 
F 0 "Y1" V 4700 3900 50  0000 R CNN
F 1 "4.096MHz" V 4700 4300 50  0000 R CNN
F 2 "Crystal:Crystal_HC49-U_Vertical" H 4700 3850 50  0001 C CNN
F 3 "http://www.ecsxtal.com/store/pdf/hc49ux.pdf" H 4700 3850 50  0001 C CNN
	1    4700 3850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E323A05
P 5350 4050
AR Path="/5E323A05" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323A05" Ref="C21"  Part="1" 
F 0 "C21" V 5121 4050 50  0000 C CNN
F 1 "22pF" V 5212 4050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5350 4050 50  0001 C CNN
F 3 "https://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2018/06/11/CL21C470JC61PNC.pdf" H 5350 4050 50  0001 C CNN
	1    5350 4050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E323A0B
P 5450 4200
AR Path="/5E323A0B" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323A0B" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 5450 3950 50  0001 C CNN
F 1 "GND" H 5455 4027 50  0000 C CNN
F 2 "" H 5450 4200 50  0001 C CNN
F 3 "" H 5450 4200 50  0001 C CNN
	1    5450 4200
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:LM2596S-3.3 U?
U 1 1 5E323A11
P 1800 6950
AR Path="/5E323A11" Ref="U?"  Part="1" 
AR Path="/5E28CC93/5E323A11" Ref="U1"  Part="1" 
F 0 "U1" H 1800 7317 50  0000 C CNN
F 1 "LM2596S-3.3" H 1800 7226 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-5_TabPin3" H 1850 6700 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2596.pdf" H 1800 6950 50  0001 C CNN
	1    1800 6950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E323A17
P 2950 6750
AR Path="/5E323A17" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323A17" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 2950 6600 50  0001 C CNN
F 1 "+3.3V" H 2965 6923 50  0000 C CNN
F 2 "" H 2950 6750 50  0001 C CNN
F 3 "" H 2950 6750 50  0001 C CNN
	1    2950 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E323A23
P 750 7050
AR Path="/5E323A23" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323A23" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 750 6800 50  0001 C CNN
F 1 "GND" H 755 6877 50  0000 C CNN
F 2 "" H 750 7050 50  0001 C CNN
F 3 "" H 750 7050 50  0001 C CNN
	1    750  7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 7250 1800 7250
$Comp
L pspice:INDUCTOR L?
U 1 1 5E323A2A
P 2600 7050
AR Path="/5E323A2A" Ref="L?"  Part="1" 
AR Path="/5E28CC93/5E323A2A" Ref="L1"  Part="1" 
F 0 "L1" H 2600 7265 50  0000 C CNN
F 1 "33µH@3A" H 2600 7174 50  0000 C CNN
F 2 "footprints:2100LL-330-H-RC" H 2600 7050 50  0001 C CNN
F 3 "https://www.digikey.com/product-detail/en/bourns-inc/2100LL-330-H-RC/M1406-ND/725953" H 2600 7050 50  0001 C CNN
	1    2600 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 6850 2300 6750
Wire Wire Line
	2300 6750 2950 6750
Wire Wire Line
	2950 6750 2950 7050
Connection ~ 2950 6750
Wire Wire Line
	1800 7250 2350 7250
Connection ~ 1800 7250
$Comp
L Device:D_Schottky_Small D?
U 1 1 5E323A36
P 2350 7150
AR Path="/5E323A36" Ref="D?"  Part="1" 
AR Path="/5E28CC93/5E323A36" Ref="D5"  Part="1" 
F 0 "D5" V 2304 7218 50  0000 L CNN
F 1 "STPS5L25" V 2395 7218 50  0000 L CNN
F 2 "Diode_THT:D_DO-201AD_P12.70mm_Horizontal" V 2350 7150 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/68/39/58/a7/fe/b4/45/ac/CD00000845.pdf/files/CD00000845.pdf/jcr:content/translations/en.CD00000845.pdf" V 2350 7150 50  0001 C CNN
	1    2350 7150
	0    1    1    0   
$EndComp
Connection ~ 2350 7250
Wire Wire Line
	2350 7250 2950 7250
$Comp
L Device:R_Small R?
U 1 1 5E323A3E
P 9750 2300
AR Path="/5E323A3E" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323A3E" Ref="R13"  Part="1" 
F 0 "R13" H 9809 2346 50  0000 L CNN
F 1 "15K" H 9809 2255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 9750 2300 50  0001 C CNN
F 3 "~" H 9750 2300 50  0001 C CNN
	1    9750 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E323A44
P 9750 2600
AR Path="/5E323A44" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323A44" Ref="R14"  Part="1" 
F 0 "R14" H 9691 2554 50  0000 R CNN
F 1 "1K" H 9691 2645 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 9750 2600 50  0001 C CNN
F 3 "~" H 9750 2600 50  0001 C CNN
	1    9750 2600
	-1   0    0    1   
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5E323A4A
P 9750 2200
AR Path="/5E323A4A" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323A4A" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 9750 2050 50  0001 C CNN
F 1 "+BATT" H 9765 2373 50  0000 C CNN
F 2 "" H 9750 2200 50  0001 C CNN
F 3 "" H 9750 2200 50  0001 C CNN
	1    9750 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E323A50
P 9750 2700
AR Path="/5E323A50" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323A50" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 9750 2450 50  0001 C CNN
F 1 "GND" H 9755 2527 50  0000 C CNN
F 2 "" H 9750 2700 50  0001 C CNN
F 3 "" H 9750 2700 50  0001 C CNN
	1    9750 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5E323A56
P 2950 7150
AR Path="/5E323A56" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323A56" Ref="C16"  Part="1" 
F 0 "C16" H 3041 7197 50  0000 L CNN
F 1 "470µF@50V" H 3041 7104 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D13.0mm_P5.00mm" H 2950 7150 50  0001 C CNN
F 3 "~" H 2950 7150 50  0001 C CNN
	1    2950 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5E323A5C
P 750 6950
AR Path="/5E323A5C" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323A5C" Ref="C1"  Part="1" 
F 0 "C1" H 841 6997 50  0000 L CNN
F 1 "470μF@50V" H 841 6904 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D13.0mm_P5.00mm" H 750 6950 50  0001 C CNN
F 3 "~" H 750 6950 50  0001 C CNN
	1    750  6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 7050 1300 7250
Wire Wire Line
	1300 7050 750  7050
Connection ~ 1300 7050
Connection ~ 750  7050
Wire Notes Line
	3550 7600 600  7600
Wire Notes Line
	3550 7600 3550 6500
Wire Wire Line
	2250 4250 2700 4250
$Comp
L Device:C_Small C?
U 1 1 5E323A6E
P 2700 4150
AR Path="/5E323A6E" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323A6E" Ref="C12"  Part="1" 
F 0 "C12" H 2792 4197 50  0000 L CNN
F 1 "100nF" H 2792 4104 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2700 4150 50  0001 C CNN
F 3 "~" H 2700 4150 50  0001 C CNN
	1    2700 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5E323A74
P 2250 4150
AR Path="/5E323A74" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323A74" Ref="C6"  Part="1" 
F 0 "C6" H 2342 4197 50  0000 L CNN
F 1 "4.7µF" H 2342 4104 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2250 4150 50  0001 C CNN
F 3 "~" H 2250 4150 50  0001 C CNN
	1    2250 4150
	1    0    0    -1  
$EndComp
Connection ~ 2700 4050
Wire Wire Line
	2700 4050 2250 4050
$Comp
L power:GNDD #PWR?
U 1 1 5E323A7C
P 2000 2950
AR Path="/5E323A7C" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323A7C" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 2000 2700 50  0001 C CNN
F 1 "GNDD" H 2005 2794 50  0000 C CNN
F 2 "" H 2000 2950 50  0001 C CNN
F 3 "" H 2000 2950 50  0001 C CNN
	1    2000 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D?
U 1 1 5E323A82
P 1650 3950
AR Path="/5E323A82" Ref="D?"  Part="1" 
AR Path="/5E28CC93/5E323A82" Ref="D3"  Part="1" 
F 0 "D3" V 1650 4050 50  0000 L CNN
F 1 "BAS16LT1G " V 1850 4000 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1650 3950 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1650 3950 50  0001 C CNN
	1    1650 3950
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D?
U 1 1 5E323A88
P 1200 3950
AR Path="/5E323A88" Ref="D?"  Part="1" 
AR Path="/5E28CC93/5E323A88" Ref="D1"  Part="1" 
F 0 "D1" V 1200 3850 50  0000 R CNN
F 1 "BAS16LT1G " V 1000 3900 50  0000 R CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1200 3950 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1200 3950 50  0001 C CNN
	1    1200 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 4050 1200 4050
Wire Wire Line
	1650 3850 1200 3850
Wire Wire Line
	2700 4050 3150 4050
$Comp
L Device:C_Small C?
U 1 1 5E323A91
P 950 4050
AR Path="/5E323A91" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323A91" Ref="C2"  Part="1" 
F 0 "C2" V 718 4050 50  0000 C CNN
F 1 "33nF" V 811 4050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 950 4050 50  0001 C CNN
F 3 "~" H 950 4050 50  0001 C CNN
	1    950  4050
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 4050 1050 4050
Connection ~ 1200 4050
$Comp
L power:GNDD #PWR?
U 1 1 5E323A99
P 750 4250
AR Path="/5E323A99" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323A99" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 750 4000 50  0001 C CNN
F 1 "GNDD" H 754 4095 50  0000 C CNN
F 2 "" H 750 4250 50  0001 C CNN
F 3 "" H 750 4250 50  0001 C CNN
	1    750  4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4050 2050 4050
Wire Wire Line
	2050 4050 2050 3950
Wire Wire Line
	2050 3950 3150 3950
Connection ~ 1650 4050
Connection ~ 2250 2950
Wire Wire Line
	2000 2950 2250 2950
$Comp
L Device:C_Small C?
U 1 1 5E323AA6
P 5350 3650
AR Path="/5E323AA6" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323AA6" Ref="C20"  Part="1" 
F 0 "C20" V 5121 3650 50  0000 C CNN
F 1 "22pF" V 5212 3650 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5350 3650 50  0001 C CNN
F 3 "https://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2018/06/11/CL21C470JC61PNC.pdf" H 5350 3650 50  0001 C CNN
	1    5350 3650
	0    1    1    0   
$EndComp
Text Notes 1350 3700 0    50   ~ 0
Array shunt and main\nvoltage measurement
$Comp
L power:GND #PWR?
U 1 1 5E323AAD
P 4600 3300
AR Path="/5E323AAD" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323AAD" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 4600 3050 50  0001 C CNN
F 1 "GND" H 4605 3127 50  0000 C CNN
F 2 "" H 4600 3300 50  0001 C CNN
F 3 "" H 4600 3300 50  0001 C CNN
	1    4600 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E323AB3
P 2250 3350
AR Path="/5E323AB3" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323AB3" Ref="R1"  Part="1" 
F 0 "R1" V 2150 3350 50  0000 C CNN
F 1 "249K" V 2250 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2250 3350 50  0001 C CNN
F 3 "CMF55249K00BEEB " H 2250 3350 50  0001 C CNN
	1    2250 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E323AB9
P 2550 3350
AR Path="/5E323AB9" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323AB9" Ref="R3"  Part="1" 
F 0 "R3" V 2450 3350 50  0000 C CNN
F 1 "1K" V 2550 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2550 3350 50  0001 C CNN
F 3 " CMF551K0000BEEB " H 2550 3350 50  0001 C CNN
	1    2550 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 3850 3150 3850
Connection ~ 2250 4250
Wire Wire Line
	2700 4250 3150 4250
Wire Wire Line
	3150 4250 3150 4150
Connection ~ 2700 4250
Wire Wire Line
	1200 4050 1200 4350
Wire Wire Line
	1200 3850 1200 3650
Connection ~ 1200 3850
Wire Wire Line
	3150 2750 3150 3200
Wire Wire Line
	3050 2950 3050 3300
Wire Wire Line
	3050 3300 3150 3300
Wire Wire Line
	2950 3150 2950 3450
Wire Wire Line
	2950 3450 3150 3450
Connection ~ 1650 3850
Wire Wire Line
	2700 3350 2700 3400
Wire Wire Line
	2700 3350 2850 3350
Wire Wire Line
	2850 3550 3150 3550
Wire Wire Line
	2850 3350 2850 3550
$Comp
L power:GNDD #PWR?
U 1 1 5E323AD7
P 2700 3400
AR Path="/5E323AD7" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323AD7" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 2700 3150 50  0001 C CNN
F 1 "GNDD" H 2700 3250 50  0000 C CNN
F 2 "" H 2700 3400 50  0001 C CNN
F 3 "" H 2700 3400 50  0001 C CNN
	1    2700 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3550 3150 3650
Wire Wire Line
	2400 3350 2400 3750
Wire Wire Line
	2400 3750 3150 3750
Connection ~ 2400 3350
$Comp
L Device:Crystal_Small Y?
U 1 1 5E323AE2
P 8550 2050
AR Path="/5E323AE2" Ref="Y?"  Part="1" 
AR Path="/5E28CC93/5E323AE2" Ref="Y2"  Part="1" 
F 0 "Y2" V 8550 2000 50  0000 L CNN
F 1 "16MHz" V 8595 2138 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-U_Vertical" H 8550 2050 50  0001 C CNN
F 3 "~" H 8550 2050 50  0001 C CNN
	1    8550 2050
	0    1    1    0   
$EndComp
Connection ~ 2700 3350
Wire Wire Line
	8300 2000 8300 1950
Wire Wire Line
	8300 1950 8550 1950
Wire Wire Line
	8300 2100 8300 2150
Wire Wire Line
	8300 2150 8550 2150
$Comp
L power:GND #PWR?
U 1 1 5E323AED
P 9500 2050
AR Path="/5E323AED" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323AED" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 9500 1800 50  0001 C CNN
F 1 "GND" H 9505 1877 50  0000 C CNN
F 2 "" H 9500 2050 50  0001 C CNN
F 3 "" H 9500 2050 50  0001 C CNN
	1    9500 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E323AF3
P 9150 1850
AR Path="/5E323AF3" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323AF3" Ref="C24"  Part="1" 
F 0 "C24" V 9050 1850 50  0000 C CNN
F 1 "22pF" V 9100 2000 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 9150 1850 50  0001 C CNN
F 3 "~" H 9150 1850 50  0001 C CNN
	1    9150 1850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E323AF9
P 9150 2250
AR Path="/5E323AF9" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323AF9" Ref="C25"  Part="1" 
F 0 "C25" V 9379 2250 50  0000 C CNN
F 1 "22pF" V 9288 2250 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 9150 2250 50  0001 C CNN
F 3 "~" H 9150 2250 50  0001 C CNN
	1    9150 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8550 1950 8550 1850
Wire Wire Line
	8550 1850 9050 1850
Connection ~ 8550 1950
Wire Wire Line
	8550 2150 8550 2250
Wire Wire Line
	8550 2250 9050 2250
Connection ~ 8550 2150
Wire Wire Line
	9250 1850 9250 2050
Wire Wire Line
	9250 2050 9500 2050
Wire Wire Line
	9250 2250 9250 2050
Connection ~ 9250 2050
Wire Wire Line
	9750 2400 9750 2450
Wire Wire Line
	9000 2300 9000 2450
Wire Wire Line
	9000 2450 9750 2450
Connection ~ 9750 2450
Wire Wire Line
	9750 2450 9750 2500
Text Label 8050 2000 0    50   ~ 0
PB6
Text Label 8050 2100 0    50   ~ 0
PB7
Text Label 8050 2300 0    50   ~ 0
PC0
Text Label 8050 2400 0    50   ~ 0
PC1
Text Label 8050 2500 0    50   ~ 0
PC2
Text Label 8050 2700 0    50   ~ 0
ENABLE_SW
Text Label 8050 2800 0    50   ~ 0
DISPLAY_CS
Text Label 8050 3100 0    50   ~ 0
RXD
Text Label 8050 3200 0    50   ~ 0
TXD
Text Label 8050 3800 0    50   ~ 0
PD7
$Comp
L Device:C_Small C?
U 1 1 5E323B19
P 4500 3200
AR Path="/5E323B19" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323B19" Ref="C18"  Part="1" 
F 0 "C18" V 4271 3200 50  0000 C CNN
F 1 "100nF" V 4362 3200 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 4500 3200 50  0001 C CNN
F 3 "~" H 4500 3200 50  0001 C CNN
	1    4500 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 3200 4250 3200
Connection ~ 4250 3200
Wire Wire Line
	4250 3200 4250 3150
Wire Wire Line
	4150 3200 4250 3200
Wire Wire Line
	4600 3300 4600 3200
Wire Wire Line
	4150 3300 4600 3300
Connection ~ 5450 4050
Wire Wire Line
	5450 4050 5450 4200
Wire Wire Line
	5450 3650 5450 4050
Wire Wire Line
	4150 3850 4500 3850
Wire Wire Line
	4500 3850 4500 3750
Wire Wire Line
	4500 3750 4700 3750
Wire Wire Line
	4700 3950 5000 3950
Wire Wire Line
	5000 3950 5000 4050
Wire Wire Line
	5000 4050 5250 4050
Connection ~ 4700 3950
Wire Wire Line
	4700 3750 5000 3750
Wire Wire Line
	5000 3750 5000 3650
Wire Wire Line
	5000 3650 5250 3650
Connection ~ 4700 3750
Text Label 6850 1400 2    50   ~ 0
AREF
$Comp
L power:GND #PWR?
U 1 1 5E323B6E
P 7900 950
AR Path="/5E323B6E" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323B6E" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 7900 700 50  0001 C CNN
F 1 "GND" H 7905 777 50  0000 C CNN
F 2 "" H 7900 950 50  0001 C CNN
F 3 "" H 7900 950 50  0001 C CNN
	1    7900 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 950  7550 950 
Connection ~ 7450 950 
$Comp
L Device:C_Small C?
U 1 1 5E323B76
P 7800 950
AR Path="/5E323B76" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323B76" Ref="C23"  Part="1" 
F 0 "C23" V 7571 950 50  0000 C CNN
F 1 "100nF" V 7662 950 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 7800 950 50  0001 C CNN
F 3 "~" H 7800 950 50  0001 C CNN
	1    7800 950 
	0    1    1    0   
$EndComp
Connection ~ 7550 950 
Wire Wire Line
	2850 7050 2950 7050
Connection ~ 2950 7050
Wire Wire Line
	2300 7050 2350 7050
Connection ~ 2350 7050
Connection ~ 4600 3300
Text Label 4150 4050 0    50   ~ 0
ADE7912_CLKOUT
Wire Wire Line
	4150 3950 4700 3950
$Comp
L main:ADE7912 U?
U 1 1 5E323BAF
P 3650 5550
AR Path="/5E323BAF" Ref="U?"  Part="1" 
AR Path="/5E28CC93/5E323BAF" Ref="U3"  Part="1" 
F 0 "U3" H 3650 6315 50  0000 C CNN
F 1 "ADE7912" H 3650 6224 50  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 3600 4800 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADE7912_7913.pdf" H 3600 4800 50  0001 C CNN
	1    3650 5550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E323BB5
P 4250 5000
AR Path="/5E323BB5" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323BB5" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 4250 4850 50  0001 C CNN
F 1 "+3.3V" H 4265 5173 50  0000 C CNN
F 2 "" H 4250 5000 50  0001 C CNN
F 3 "" H 4250 5000 50  0001 C CNN
	1    4250 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E323BBB
P 4200 6050
AR Path="/5E323BBB" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323BBB" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 4200 5800 50  0001 C CNN
F 1 "GND" H 4205 5877 50  0000 C CNN
F 2 "" H 4200 6050 50  0001 C CNN
F 3 "" H 4200 6050 50  0001 C CNN
	1    4200 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 6000 4200 6000
Wire Wire Line
	4200 6000 4200 6050
Text Label 4150 5600 0    50   ~ 0
MISO
Text Label 4150 5500 0    50   ~ 0
MOSI
Text Label 4150 5400 0    50   ~ 0
SCK
$Comp
L power:GNDD #PWR?
U 1 1 5E323BC6
P 2250 6100
AR Path="/5E323BC6" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323BC6" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 2250 5850 50  0001 C CNN
F 1 "GNDD" H 2254 5945 50  0000 C CNN
F 2 "" H 2250 6100 50  0001 C CNN
F 3 "" H 2250 6100 50  0001 C CNN
	1    2250 6100
	1    0    0    -1  
$EndComp
Connection ~ 2700 5900
$Comp
L Device:D_Small D?
U 1 1 5E323BCD
P 1650 5800
AR Path="/5E323BCD" Ref="D?"  Part="1" 
AR Path="/5E28CC93/5E323BCD" Ref="D4"  Part="1" 
F 0 "D4" V 1650 5900 50  0000 L CNN
F 1 "BAS16LT1G " V 1850 5850 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1650 5800 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1650 5800 50  0001 C CNN
	1    1650 5800
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D?
U 1 1 5E323BD3
P 1200 5800
AR Path="/5E323BD3" Ref="D?"  Part="1" 
AR Path="/5E28CC93/5E323BD3" Ref="D2"  Part="1" 
F 0 "D2" V 1200 5700 50  0000 R CNN
F 1 "BAS16LT1G " V 1000 5750 50  0000 R CNN
F 2 "Diode_SMD:D_SOT-23_ANK" V 1200 5800 50  0001 C CNN
F 3 "www.onsemi.com/pub/Collateral/BAS16LT1-D.PDF" V 1200 5800 50  0001 C CNN
	1    1200 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 5900 1200 5900
Wire Wire Line
	1650 5700 1200 5700
Wire Wire Line
	2700 5900 3150 5900
Connection ~ 1200 5900
Wire Wire Line
	1650 5900 2050 5900
Wire Wire Line
	2050 5900 2050 5800
Wire Wire Line
	2050 5800 3150 5800
Connection ~ 1650 5900
Text Notes 3100 6450 0    50   Italic 0
*V2P only available for ADE7913
$Comp
L power:GND #PWR?
U 1 1 5E323BF0
P 4600 5150
AR Path="/5E323BF0" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323BF0" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 4600 4900 50  0001 C CNN
F 1 "GND" H 4605 4977 50  0000 C CNN
F 2 "" H 4600 5150 50  0001 C CNN
F 3 "" H 4600 5150 50  0001 C CNN
	1    4600 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5700 3150 5700
Wire Wire Line
	2700 6100 3150 6100
Wire Wire Line
	3150 6100 3150 6000
Connection ~ 2700 6100
Wire Wire Line
	1200 5900 1200 6200
Wire Wire Line
	1200 5700 1200 5500
Connection ~ 1200 5700
Wire Wire Line
	3150 4600 3150 5050
Wire Wire Line
	3050 4800 3050 5150
Wire Wire Line
	3050 5150 3150 5150
Wire Wire Line
	2950 5000 2950 5300
Wire Wire Line
	2950 5300 3150 5300
Connection ~ 1650 5700
Wire Wire Line
	2850 5400 3150 5400
$Comp
L power:GNDD #PWR?
U 1 1 5E323C08
P 2700 5250
AR Path="/5E323C08" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323C08" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 2700 5000 50  0001 C CNN
F 1 "GNDD" H 2700 5100 50  0000 C CNN
F 2 "" H 2700 5250 50  0001 C CNN
F 3 "" H 2700 5250 50  0001 C CNN
	1    2700 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 5400 3150 5500
Wire Wire Line
	2400 5600 3150 5600
$Comp
L Device:C_Small C?
U 1 1 5E323C11
P 4500 5050
AR Path="/5E323C11" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C11" Ref="C19"  Part="1" 
F 0 "C19" V 4271 5050 50  0000 C CNN
F 1 "100nF" V 4362 5050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 4500 5050 50  0001 C CNN
F 3 "~" H 4500 5050 50  0001 C CNN
	1    4500 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 5050 4250 5050
Connection ~ 4250 5050
Wire Wire Line
	4250 5050 4250 5000
Wire Wire Line
	4150 5050 4250 5050
Wire Wire Line
	4600 5150 4600 5050
Wire Wire Line
	4150 5150 4600 5150
Connection ~ 4600 5150
Text Label 4150 5800 0    50   ~ 0
ADE7912_CLKOUT
Text Notes 1050 4950 0    50   ~ 0
Motor shunt and \nunassigned
$Comp
L Device:CP_Small C?
U 1 1 5E323C20
P 2250 3050
AR Path="/5E323C20" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C20" Ref="C5"  Part="1" 
F 0 "C5" H 2050 3000 50  0000 L CNN
F 1 "4.7µF" H 1950 3100 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2250 3050 50  0001 C CNN
F 3 "~" H 2250 3050 50  0001 C CNN
	1    2250 3050
	-1   0    0    1   
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5E323C26
P 2250 2850
AR Path="/5E323C26" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C26" Ref="C4"  Part="1" 
F 0 "C4" H 2342 2897 50  0000 L CNN
F 1 "10µF" H 2342 2804 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2250 2850 50  0001 C CNN
F 3 "~" H 2250 2850 50  0001 C CNN
	1    2250 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2950 3050 2950
Wire Wire Line
	2700 2950 2250 2950
Connection ~ 2700 2950
Wire Wire Line
	2700 3150 2950 3150
Wire Wire Line
	2250 3150 2700 3150
Connection ~ 2700 3150
$Comp
L Device:C_Small C?
U 1 1 5E323C32
P 2700 3050
AR Path="/5E323C32" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C32" Ref="C11"  Part="1" 
F 0 "C11" H 2792 3097 50  0000 L CNN
F 1 "100nF" H 2792 3004 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2700 3050 50  0001 C CNN
F 3 "~" H 2700 3050 50  0001 C CNN
	1    2700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5200 2400 5600
Wire Wire Line
	2850 5200 2850 5400
Wire Wire Line
	2700 5200 2850 5200
Wire Wire Line
	2700 5200 2700 5250
Connection ~ 2700 5200
$Comp
L Device:R R?
U 1 1 5E323C3E
P 2550 5200
AR Path="/5E323C3E" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323C3E" Ref="R4"  Part="1" 
F 0 "R4" V 2550 5200 50  0000 C CNN
F 1 "?" V 2550 5200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2550 5200 50  0001 C CNN
F 3 "" H 2550 5200 50  0001 C CNN
	1    2550 5200
	0    -1   -1   0   
$EndComp
Connection ~ 2400 5200
$Comp
L Device:R R?
U 1 1 5E323C45
P 2250 5200
AR Path="/5E323C45" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323C45" Ref="R2"  Part="1" 
F 0 "R2" V 2250 5200 50  0000 C CNN
F 1 "?" V 2250 5200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 2250 5200 50  0001 C CNN
F 3 "" H 2250 5200 50  0001 C CNN
	1    2250 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5E323C4C
P 2250 6000
AR Path="/5E323C4C" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C4C" Ref="C9"  Part="1" 
F 0 "C9" H 2342 6047 50  0000 L CNN
F 1 "4.7µF" H 2342 5954 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2250 6000 50  0001 C CNN
F 3 "~" H 2250 6000 50  0001 C CNN
	1    2250 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6100 2700 6100
Wire Wire Line
	2700 5900 2250 5900
$Comp
L Device:C_Small C?
U 1 1 5E323C54
P 2700 6000
AR Path="/5E323C54" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C54" Ref="C15"  Part="1" 
F 0 "C15" H 2792 6047 50  0000 L CNN
F 1 "100nF" H 2792 5954 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2700 6000 50  0001 C CNN
F 3 "~" H 2700 6000 50  0001 C CNN
	1    2700 6000
	1    0    0    -1  
$EndComp
Connection ~ 2250 6100
NoConn ~ 4150 5700
$Comp
L power:GNDD #PWR?
U 1 1 5E323C5C
P 2000 4800
AR Path="/5E323C5C" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323C5C" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 2000 4550 50  0001 C CNN
F 1 "GNDD" H 2005 4644 50  0000 C CNN
F 2 "" H 2000 4800 50  0001 C CNN
F 3 "" H 2000 4800 50  0001 C CNN
	1    2000 4800
	1    0    0    -1  
$EndComp
Connection ~ 2250 4800
Wire Wire Line
	2000 4800 2250 4800
$Comp
L Device:CP_Small C?
U 1 1 5E323C64
P 2250 4900
AR Path="/5E323C64" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C64" Ref="C8"  Part="1" 
F 0 "C8" H 2050 4850 50  0000 L CNN
F 1 "4.7µF" H 1950 4950 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2250 4900 50  0001 C CNN
F 3 "~" H 2250 4900 50  0001 C CNN
	1    2250 4900
	-1   0    0    1   
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5E323C6A
P 2250 4700
AR Path="/5E323C6A" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C6A" Ref="C7"  Part="1" 
F 0 "C7" H 2342 4747 50  0000 L CNN
F 1 "10µF" H 2342 4654 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2250 4700 50  0001 C CNN
F 3 "~" H 2250 4700 50  0001 C CNN
	1    2250 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 4800 3050 4800
Wire Wire Line
	2700 4800 2250 4800
Connection ~ 2700 4800
Wire Wire Line
	2700 4600 3150 4600
Wire Wire Line
	2700 4600 2250 4600
Connection ~ 2700 4600
$Comp
L Device:C_Small C?
U 1 1 5E323C76
P 2700 4700
AR Path="/5E323C76" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C76" Ref="C13"  Part="1" 
F 0 "C13" H 2792 4747 50  0000 L CNN
F 1 "100nF" H 2792 4654 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2700 4700 50  0001 C CNN
F 3 "~" H 2700 4700 50  0001 C CNN
	1    2700 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 5000 2950 5000
Wire Wire Line
	2250 5000 2700 5000
Connection ~ 2700 5000
$Comp
L Device:C_Small C?
U 1 1 5E323C7F
P 2700 4900
AR Path="/5E323C7F" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C7F" Ref="C14"  Part="1" 
F 0 "C14" H 2792 4947 50  0000 L CNN
F 1 "100nF" H 2792 4854 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2700 4900 50  0001 C CNN
F 3 "~" H 2700 4900 50  0001 C CNN
	1    2700 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E323C85
P 6850 1600
AR Path="/5E323C85" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323C85" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 6850 1350 50  0001 C CNN
F 1 "GND" H 6855 1427 50  0000 C CNN
F 2 "" H 6850 1600 50  0001 C CNN
F 3 "" H 6850 1600 50  0001 C CNN
	1    6850 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 2000 8300 2000
Wire Wire Line
	7450 1100 7450 950 
Wire Wire Line
	8050 2300 9000 2300
$Comp
L power:GND #PWR?
U 1 1 5E323C8E
P 7450 4100
AR Path="/5E323C8E" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323C8E" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 7450 3850 50  0001 C CNN
F 1 "GND" H 7455 3927 50  0000 C CNN
F 2 "" H 7450 4100 50  0001 C CNN
F 3 "" H 7450 4100 50  0001 C CNN
	1    7450 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E323C94
P 6850 1500
AR Path="/5E323C94" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323C94" Ref="C22"  Part="1" 
F 0 "C22" H 6650 1550 50  0000 C CNN
F 1 "100nF" H 6700 1450 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6850 1500 50  0001 C CNN
F 3 "~" H 6850 1500 50  0001 C CNN
	1    6850 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 950  7550 1100
Wire Wire Line
	8050 2100 8300 2100
Text Label 8050 3700 0    50   ~ 0
PD6
Text Label 8050 3300 0    50   ~ 0
RADIO_IRQ
Wire Wire Line
	7700 950  7550 950 
$Comp
L main-rescue:ATmega328P-PU-MCU_Microchip_ATmega U?
U 1 1 5E323CA3
P 7450 2600
AR Path="/5E323CA3" Ref="U?"  Part="1" 
AR Path="/5E28CC93/5E323CA3" Ref="U5"  Part="1" 
F 0 "U5" H 6809 2646 50  0000 R CNN
F 1 "ATmega328P-PU" H 6809 2555 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 7450 2600 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 7450 2600 50  0001 C CNN
	1    7450 2600
	1    0    0    -1  
$EndComp
Text Label 8050 1900 0    50   ~ 0
SCK
Text Label 8050 1700 0    50   ~ 0
MOSI
Text Label 8050 1800 0    50   ~ 0
MISO
Text Label 8050 1600 0    50   ~ 0
RADIO_CS
Text Label 8050 3400 0    50   ~ 0
ADC_IRQ
Text Label 8050 3500 0    50   ~ 0
PD4
Text Label 8050 3600 0    50   ~ 0
PD5
$Comp
L power:GNDD #PWR?
U 1 1 5E323CB0
P 900 1250
AR Path="/5E323CB0" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323CB0" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 900 1000 50  0001 C CNN
F 1 "GNDD" H 905 1094 50  0000 C CNN
F 2 "" H 900 1250 50  0001 C CNN
F 3 "" H 900 1250 50  0001 C CNN
	1    900  1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E323CB9
P 950 2300
AR Path="/5E323CB9" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323CB9" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 950 2050 50  0001 C CNN
F 1 "GND" H 955 2127 50  0000 C CNN
F 2 "" H 950 2300 50  0001 C CNN
F 3 "" H 950 2300 50  0001 C CNN
	1    950  2300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5E323CBF
P 750 2200
AR Path="/5E323CBF" Ref="J?"  Part="1" 
AR Path="/5E28CC93/5E323CBF" Ref="J2"  Part="1" 
F 0 "J2" H 750 1700 50  0000 C CNN
F 1 "Screw Terminal 01x03" H 400 1800 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 750 2200 50  0001 C CNN
F 3 "~" H 750 2200 50  0001 C CNN
	1    750  2200
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E323CC5
P 1200 2200
AR Path="/5E323CC5" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323CC5" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 1200 2050 50  0001 C CNN
F 1 "+3.3V" H 1215 2373 50  0000 C CNN
F 2 "" H 1200 2200 50  0001 C CNN
F 3 "" H 1200 2200 50  0001 C CNN
	1    1200 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  2200 1200 2200
$Comp
L power:GND #PWR?
U 1 1 5E323CD2
P 8600 4100
AR Path="/5E323CD2" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323CD2" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 8600 3850 50  0001 C CNN
F 1 "GND" H 8605 3927 50  0000 C CNN
F 2 "" H 8600 4100 50  0001 C CNN
F 3 "" H 8600 4100 50  0001 C CNN
	1    8600 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E323CDE
P 8450 3800
AR Path="/5E323CDE" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323CDE" Ref="R6"  Part="1" 
F 0 "R6" V 8550 3650 50  0000 C CNN
F 1 "1K" V 8450 3800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8380 3800 50  0001 C CNN
F 3 "~" H 8450 3800 50  0001 C CNN
	1    8450 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	8300 3800 8050 3800
$Comp
L Device:R R?
U 1 1 5E323CF8
P 8800 2900
AR Path="/5E323CF8" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E323CF8" Ref="R8"  Part="1" 
F 0 "R8" V 8700 2850 50  0000 L CNN
F 1 "10K" V 8800 2900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8730 2900 50  0001 C CNN
F 3 "~" H 8800 2900 50  0001 C CNN
	1    8800 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 2900 8650 2900
$Comp
L power:+3.3V #PWR?
U 1 1 5E323CFF
P 8950 2900
AR Path="/5E323CFF" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323CFF" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 8950 2750 50  0001 C CNN
F 1 "+3.3V" H 8965 3073 50  0000 C CNN
F 2 "" H 8950 2900 50  0001 C CNN
F 3 "" H 8950 2900 50  0001 C CNN
	1    8950 2900
	1    0    0    -1  
$EndComp
Text Label 4150 5900 0    50   ~ 0
ADC_IRQ
Text Label 4150 3450 0    50   ~ 0
ADC1_CS
Wire Wire Line
	2700 2750 3150 2750
Wire Wire Line
	2700 2750 2250 2750
Connection ~ 2700 2750
$Comp
L Device:C_Small C?
U 1 1 5E323D13
P 2700 2850
AR Path="/5E323D13" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323D13" Ref="C10"  Part="1" 
F 0 "C10" H 2792 2897 50  0000 L CNN
F 1 "100nF" H 2792 2804 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2700 2850 50  0001 C CNN
F 3 "~" H 2700 2850 50  0001 C CNN
	1    2700 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  6850 1300 6850
Text HLabel 900  950  2    50   Input ~ 0
BATTERY_SENSOR
Text HLabel 1850 3350 0    50   Input ~ 0
BATTERY_SENSOR
Wire Wire Line
	1850 3350 2100 3350
Text HLabel 900  1050 2    50   Input ~ 0
ARRAY_SHUNT
Text HLabel 900  1150 2    50   Input ~ 0
MOTOR_SHUNT
$Comp
L Connector:Screw_Terminal_01x05 J?
U 1 1 5E3239C5
P 700 1050
AR Path="/5E3239C5" Ref="J?"  Part="1" 
AR Path="/5E28CC93/5E3239C5" Ref="J1"  Part="1" 
F 0 "J1" H 700 600 50  0000 C CNN
F 1 "Screw Terminal 01x06" H 350 700 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-5_P5.08mm" H 700 1050 50  0001 C CNN
F 3 "~" H 700 1050 50  0001 C CNN
	1    700  1050
	-1   0    0    1   
$EndComp
Text HLabel 1200 4350 2    50   Input ~ 0
ARRAY_SHUNT
Text HLabel 900  850  2    50   Input ~ 0
MISCELLANEOUS
Text HLabel 1850 5200 0    50   Input ~ 0
MISCELLANEOUS
Wire Wire Line
	2100 5200 1850 5200
Text HLabel 1200 6200 2    50   Input ~ 0
MOTOR_SHUNT
$Comp
L Device:R R?
U 1 1 5E5476FC
P 9000 3300
AR Path="/5E5476FC" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E5476FC" Ref="R9"  Part="1" 
F 0 "R9" V 8900 3300 50  0000 C CNN
F 1 "10K" V 9000 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8930 3300 50  0001 C CNN
F 3 "~" H 9000 3300 50  0001 C CNN
	1    9000 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E54E20A
P 9000 3400
AR Path="/5E54E20A" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E54E20A" Ref="R10"  Part="1" 
F 0 "R10" V 9100 3400 50  0000 C CNN
F 1 "10K" V 9000 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8930 3400 50  0001 C CNN
F 3 "~" H 9000 3400 50  0001 C CNN
	1    9000 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 3400 8850 3400
Wire Wire Line
	8050 3300 8850 3300
$Comp
L power:GND #PWR?
U 1 1 5E5BAD30
P 9150 3500
AR Path="/5E5BAD30" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E5BAD30" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 9150 3250 50  0001 C CNN
F 1 "GND" H 9250 3400 50  0000 C CNN
F 2 "" H 9150 3500 50  0001 C CNN
F 3 "" H 9150 3500 50  0001 C CNN
	1    9150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3300 9150 3400
$Comp
L Device:R R?
U 1 1 5E5D8C46
P 8450 3700
AR Path="/5E5D8C46" Ref="R?"  Part="1" 
AR Path="/5E28CC93/5E5D8C46" Ref="R5"  Part="1" 
F 0 "R5" V 8350 3700 50  0000 C CNN
F 1 "1K" V 8450 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8380 3700 50  0001 C CNN
F 3 "~" H 8450 3700 50  0001 C CNN
	1    8450 3700
	0    1    1    0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5E323CD8
P 8600 3950
AR Path="/5E323CD8" Ref="D?"  Part="1" 
AR Path="/5E28CC93/5E323CD8" Ref="D6"  Part="1" 
F 0 "D6" V 8550 4100 50  0000 R CNN
F 1 "LED" V 8450 4150 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8600 3950 50  0001 C CNN
F 3 "~" H 8600 3950 50  0001 C CNN
	1    8600 3950
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5E5FB19D
P 8900 3950
AR Path="/5E5FB19D" Ref="D?"  Part="1" 
AR Path="/5E28CC93/5E5FB19D" Ref="D7"  Part="1" 
F 0 "D7" V 8940 3829 50  0000 R CNN
F 1 "LED" V 8847 3829 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8900 3950 50  0001 C CNN
F 3 "~" H 8900 3950 50  0001 C CNN
	1    8900 3950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E5FE29C
P 8900 4100
AR Path="/5E5FE29C" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E5FE29C" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 8900 3850 50  0001 C CNN
F 1 "GND" H 8905 3927 50  0000 C CNN
F 2 "" H 8900 4100 50  0001 C CNN
F 3 "" H 8900 4100 50  0001 C CNN
	1    8900 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3700 8300 3700
Wire Wire Line
	8600 3700 8900 3700
Wire Wire Line
	8900 3700 8900 3800
Wire Wire Line
	9150 3500 9150 3400
Connection ~ 9150 3400
Text Label 8050 1500 0    50   ~ 0
ADC1_CS
Text Label 8050 1400 0    50   ~ 0
ADC2_CS
Text Label 4150 5300 0    50   ~ 0
ADC2_CS
$Comp
L Device:R R7
U 1 1 5E7C8B82
P 8750 1250
F 0 "R7" H 8820 1297 50  0000 L CNN
F 1 "10K" H 8820 1204 50  0000 L CNN
F 2 "" V 8680 1250 50  0001 C CNN
F 3 "~" H 8750 1250 50  0001 C CNN
	1    8750 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5E7DA879
P 9050 1250
F 0 "R11" H 9120 1297 50  0000 L CNN
F 1 "10K" H 9120 1204 50  0000 L CNN
F 2 "" V 8980 1250 50  0001 C CNN
F 3 "~" H 9050 1250 50  0001 C CNN
	1    9050 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5E7DAC56
P 9350 1250
F 0 "R12" H 9420 1297 50  0000 L CNN
F 1 "10K" H 9420 1204 50  0000 L CNN
F 2 "" V 9280 1250 50  0001 C CNN
F 3 "~" H 9350 1250 50  0001 C CNN
	1    9350 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 1400 8750 1400
Wire Wire Line
	8050 1500 9050 1500
Wire Wire Line
	9050 1500 9050 1400
Wire Wire Line
	8050 1600 9350 1600
Wire Wire Line
	9350 1600 9350 1400
$Comp
L power:+3.3V #PWR?
U 1 1 5E8052A3
P 9050 1100
AR Path="/5E8052A3" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E8052A3" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 9050 950 50  0001 C CNN
F 1 "+3.3V" H 9065 1273 50  0000 C CNN
F 2 "" H 9050 1100 50  0001 C CNN
F 3 "" H 9050 1100 50  0001 C CNN
	1    9050 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1100 9050 1100
Connection ~ 9050 1100
Wire Wire Line
	9050 1100 9350 1100
Connection ~ 3150 3550
Connection ~ 3150 5400
Wire Wire Line
	1200 3650 750  3650
Wire Wire Line
	750  3650 750  4050
Wire Wire Line
	850  4050 750  4050
Connection ~ 750  4050
Wire Wire Line
	750  4050 750  4250
$Comp
L Device:C_Small C?
U 1 1 5EB0477E
P 950 5900
AR Path="/5EB0477E" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5EB0477E" Ref="C3"  Part="1" 
F 0 "C3" V 718 5900 50  0000 C CNN
F 1 "33nF" V 811 5900 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 950 5900 50  0001 C CNN
F 3 "~" H 950 5900 50  0001 C CNN
	1    950  5900
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 5900 1050 5900
$Comp
L power:GNDD #PWR?
U 1 1 5EB04785
P 750 6100
AR Path="/5EB04785" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5EB04785" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 750 5850 50  0001 C CNN
F 1 "GNDD" H 754 5945 50  0000 C CNN
F 2 "" H 750 6100 50  0001 C CNN
F 3 "" H 750 6100 50  0001 C CNN
	1    750  6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 5500 750  5500
Wire Wire Line
	750  5500 750  5900
Wire Wire Line
	850  5900 750  5900
Connection ~ 750  5900
Wire Wire Line
	750  5900 750  6100
$Comp
L RF_Module:RFM95W-868S2 U?
U 1 1 5E323B81
P 4250 1550
AR Path="/5E323B81" Ref="U?"  Part="1" 
AR Path="/5E28CC93/5E323B81" Ref="U4"  Part="1" 
F 0 "U4" H 3850 2200 50  0000 C CNN
F 1 "RFM95W-868S2" H 3750 2100 50  0000 C CNN
F 2 "footprints:RFM9x" H 950 3200 50  0001 C CNN
F 3 "http://www.hoperf.com/upload/rf/RFM95_96_97_98W.pdf" H 950 3200 50  0001 C CNN
	1    4250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 900  4250 1050
NoConn ~ 4750 1450
NoConn ~ 4750 1550
NoConn ~ 4750 1650
NoConn ~ 4750 1750
NoConn ~ 4750 1850
Text Label 3750 1750 2    50   ~ 0
RESET
Text Label 3750 1550 2    50   ~ 0
RADIO_CS
Text Label 3750 1250 2    50   ~ 0
SCK
Text Label 3750 1350 2    50   ~ 0
MOSI
Text Label 3750 1450 2    50   ~ 0
MISO
Connection ~ 4250 900 
Wire Wire Line
	4400 900  4250 900 
$Comp
L Device:C_Small C?
U 1 1 5E323BA0
P 4500 900
AR Path="/5E323BA0" Ref="C?"  Part="1" 
AR Path="/5E28CC93/5E323BA0" Ref="C17"  Part="1" 
F 0 "C17" V 4271 900 50  0000 C CNN
F 1 "100nF" V 4362 900 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 4500 900 50  0001 C CNN
F 3 "~" H 4500 900 50  0001 C CNN
	1    4500 900 
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E323B99
P 4600 900
AR Path="/5E323B99" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323B99" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 4600 650 50  0001 C CNN
F 1 "GND" H 4605 727 50  0000 C CNN
F 2 "" H 4600 900 50  0001 C CNN
F 3 "" H 4600 900 50  0001 C CNN
	1    4600 900 
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E323B93
P 4250 900
AR Path="/5E323B93" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323B93" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 4250 750 50  0001 C CNN
F 1 "+3.3V" H 4265 1073 50  0000 C CNN
F 2 "" H 4250 900 50  0001 C CNN
F 3 "" H 4250 900 50  0001 C CNN
	1    4250 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 2150 4350 2150
Connection ~ 4250 2150
Wire Wire Line
	4150 2150 4250 2150
$Comp
L power:GND #PWR?
U 1 1 5E323B8A
P 4250 2150
AR Path="/5E323B8A" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E323B8A" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 4250 1900 50  0001 C CNN
F 1 "GND" H 4255 1977 50  0000 C CNN
F 2 "" H 4250 2150 50  0001 C CNN
F 3 "" H 4250 2150 50  0001 C CNN
	1    4250 2150
	1    0    0    -1  
$EndComp
Wire Notes Line
	3350 2550 3350 550 
Text Notes 3300 2500 2    100  ~ 20
Connectors
Wire Notes Line
	600  6500 5600 6500
Wire Notes Line
	600  2550 5600 2550
Text Notes 5550 6450 2    100  ~ 20
Measurements
$Sheet
S 7450 5250 900  600 
U 5EE5284A
F0 "User Interface" 50
F1 "user_interface.sch" 50
F2 "MOSI" I R 8350 5550 50 
F3 "DISPLAY_CS" I R 8350 5350 50 
F4 "SCK" I R 8350 5450 50 
F5 "ENABLE_SW" I R 8350 5650 50 
F6 "LOWPOWERMODE_SW" I R 8350 5750 50 
$EndSheet
Text Label 8350 5450 0    50   ~ 0
SCK
Text Label 8350 5550 0    50   ~ 0
MOSI
Text Label 8350 5350 0    50   ~ 0
DISPLAY_CS
Text Label 4750 1950 0    50   ~ 0
RADIO_IRQ
Text Notes 5550 2500 2    100  ~ 20
Radio
Wire Notes Line
	5600 550  5600 6500
Wire Notes Line
	600  550  5600 550 
Wire Notes Line
	600  550  600  7600
Text Notes 3500 7550 2    100  ~ 20
Power Supply
Text Label 8350 5650 0    50   ~ 0
ENABLE_SW
Text Label 8050 2600 0    50   ~ 0
LOWPOWERMODE_SW
Text Label 8350 5750 0    50   ~ 0
LOWPOWERMODE_SW
$Comp
L power:+3.3V #PWR?
U 1 1 5E3239E9
P 1950 900
AR Path="/5E3239E9" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E3239E9" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 1950 750 50  0001 C CNN
F 1 "+3.3V" H 1965 1073 50  0000 C CNN
F 2 "" H 1950 900 50  0001 C CNN
F 3 "" H 1950 900 50  0001 C CNN
	1    1950 900 
	1    0    0    -1  
$EndComp
Text Label 2450 1500 0    50   ~ 0
RESET
Text Label 2450 1400 0    50   ~ 0
SCK
Text Label 2450 1300 0    50   ~ 0
MOSI
Text Label 2450 1200 0    50   ~ 0
MISO
$Comp
L power:GND #PWR?
U 1 1 5E3239DF
P 1950 1800
AR Path="/5E3239DF" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5E3239DF" Ref="#PWR014"  Part="1" 
F 0 "#PWR014" H 1950 1550 50  0001 C CNN
F 1 "GND" H 1955 1627 50  0000 C CNN
F 2 "" H 1950 1800 50  0001 C CNN
F 3 "" H 1950 1800 50  0001 C CNN
	1    1950 1800
	1    0    0    -1  
$EndComp
$Comp
L main-rescue:AVR-ISP-6-Connector J?
U 1 1 5E3239CB
P 2050 1400
AR Path="/5E3239CB" Ref="J?"  Part="1" 
AR Path="/5E28CC93/5E3239CB" Ref="J3"  Part="1" 
F 0 "J3" H 2200 1900 50  0000 R CNN
F 1 "AVR-ISP-6" H 2400 1800 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" V 1800 1450 50  0001 C CNN
F 3 " ~" H 775 850 50  0001 C CNN
	1    2050 1400
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F529563
P 950 2100
AR Path="/5F529563" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5F529563" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 950 1950 50  0001 C CNN
F 1 "+BATT" H 965 2273 50  0000 C CNN
F 2 "" H 950 2100 50  0001 C CNN
F 3 "" H 950 2100 50  0001 C CNN
	1    950  2100
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F53B631
P 750 6850
AR Path="/5F53B631" Ref="#PWR?"  Part="1" 
AR Path="/5E28CC93/5F53B631" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 750 6700 50  0001 C CNN
F 1 "+BATT" H 765 7023 50  0000 C CNN
F 2 "" H 750 6850 50  0001 C CNN
F 3 "" H 750 6850 50  0001 C CNN
	1    750  6850
	1    0    0    -1  
$EndComp
NoConn ~ 4750 1250
$EndSCHEMATC
