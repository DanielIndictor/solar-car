This directory contains the kicad files for the schematic of the telemetry system. A PDF version is offered [here](./2020-03-12 - Schematic.pdf).
