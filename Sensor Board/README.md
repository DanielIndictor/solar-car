This folder contains the files used to design and program the telemetry board.

- [board](./board) contains the kicad files and a PDF schematic of the board.
- [code](./code) contains the C/C++ code used to program the microcontroller.
- [pdfs](./pdfs) contains the datasheets of the parts used to keep them all in one place.
- [data](./data) contains some extraneous calculations used to characterize operation of the radio.
