#ifndef INTERNAL_ADC_HPP
#define INTERNAL_ADC_HPP

#include <stdint.h>

class internal_adc
{
public:
  internal_adc();

  void begin_voltage_reading();

  uint16_t await_reading();
};

#endif // INTERNAL_ADC_HPP
