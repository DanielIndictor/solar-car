#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>

// Setup information TODO: integrate slave mode.
#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define SPI_PIN_SCK PB5
#define SPI_PIN_MOSI PB3
#define SPI_PIN_MISO PB4
#define SPI_PIN_SS PB2

/**
 * @brief spi_init Enables SPI (and sets associated pins as outputs/inputs) and
 * sets microcontroller to master. Leaves CPOL/CPHA.
 */
void
spi_init();

uint8_t
spi_transcieve_byte(const uint8_t data);

inline void
spi_disable()
{
  SPCR &= ~(1 << SPIE);
}

inline uint8_t
spi_data()
{
  return SPDR;
}

void
spi_send_bytes(const uint8_t* data, const uint8_t length);

#endif /* SPI_H_ */
