#include "led_display_driver.hpp"

#include "spi.hpp"

led_display_driver::led_display_driver()
{
  spi_init();
  reconfigure_spi();

  // Configure slave select as output.
  MAX7219_DDR |= (1 << MAX7219_CS);

  // Configuration is saved in CONFIG variable.
  // We set each of these registers to the appropriate code, one by one.
  write_register_to_all(MAX7219_DECODEMODE, 0);
  // Max intensity.
  write_register_to_all(MAX7219_INTENSITY, 0x1);
  // All 8 digits connected.
  write_register_to_all(MAX7219_SCANLIMIT, 0x7);
  // Turn off display test mode.
  write_register_to_all(MAX7219_DISPLAYTEST, 0);
  // Turn on.
  write_register_to_all(MAX7219_SHUTDOWN, 1);
}

void
led_display_driver::write_digit(uint8_t digit, uint8_t val)
{
  // If the digit is >= 8 that means it goes to the second driver..
  write_register(MAX7219_DIG(digit % 8), val, (digit >= 8));
}

void
led_display_driver::write_register(uint8_t reg, uint8_t val, uint8_t driver)
{
  select();
  if (!driver)
  {
    spi_transcieve_byte(MAX7219_NOOP);
    spi_transcieve_byte(0);
  }
  spi_transcieve_byte(reg);
  spi_transcieve_byte(val);
  if (driver)
  {
    spi_transcieve_byte(MAX7219_NOOP);
    spi_transcieve_byte(0);
  }
  deselect();
}

void
led_display_driver::write_register_to_all(uint8_t reg, uint8_t val)
{
  // TODO: Check if this function works.
  // Didn't seem to update on git commit.
  select();
  spi_transcieve_byte(reg);
  spi_transcieve_byte(val);
  spi_transcieve_byte(reg);
  spi_transcieve_byte(val);
  deselect();
}

void
led_display_driver::select()
{
  MAX7219_PORT &= ~(1 << MAX7219_CS);
}

void
led_display_driver::deselect()
{
  MAX7219_PORT |= (1 << MAX7219_CS);
}
