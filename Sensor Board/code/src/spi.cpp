/*
 * spi.c
 *
 *  Created on: Jan 22, 2018
 *      Author: Daniel Indictor
 *
 */

#include "spi.hpp"

void
spi_init()
{
  // MOSI, SCK, SS as output.
  SPI_DDR |= (1 << SPI_PIN_SCK) | (1 << SPI_PIN_MOSI) | (1 << SPI_PIN_SS);
  // MISO as input.
  SPI_DDR &= ~(1 << SPI_PIN_MISO);

  // SPI configure (Master, SPI enable)
  SPCR |= (1 << MSTR) | (1 << SPE);
}

uint8_t
spi_transcieve_byte(const uint8_t data)
{
  SPDR = data;
  // Wait for reception complete.
  while (!(SPSR & (1 << SPIF)))
    ;
  return SPDR;
}


void
spi_send_bytes(const uint8_t* data, const uint8_t length)
{
  auto end = data + length;
  for (; data != end; ++data)
  {
    SPDR = *data;
    // Wait for reception complete.
    while (!(SPSR & (1 << SPIF)))
      ;
  }
}
