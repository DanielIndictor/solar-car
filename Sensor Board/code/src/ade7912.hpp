#ifndef ADE7912_HPP
#define ADE7912_HPP

#include "spi.hpp"

#include <avr/io.h>

#define ADE7912_PORT PORTB
#define ADE7912_DDR DDRB
#define ADE7912_CS 2
#define ADE7912_READ _BV(2)
#define ADE7912_WRITE 0
#define ADE7912_DUMMY 0xff
#define ADE7912_MAX_WRITE_ATTEMPTS 100

#define ADE7912_IWV (0x0 << 3)
#define ADE7912_V1WV (0x1 << 3)
#define ADE7912_V2WV (0x2 << 3)
#define ADE7912_ADC_CRC (0x4 << 3)
#define ADE7912_CTRL_CRC (0x5 << 3)
#define ADE7912_CNT_SNAPSHOT (0x7 << 3)
#define ADE7912_CONFIG (0x8 << 3)
#define ADE7912_STATUS0 (0x9 << 3)
#define ADE7912_LOCK (0xA << 3)
#define ADE7912_SYNC_SNAP (0xB << 3)
#define ADE7912_COUNTER0 (0xC << 3)
#define ADE7912_COUNTER1 (0xD << 3)
#define ADE7912_EMI_CTRL (0xE << 3)
#define ADE7912_STATUS1 (0xF << 3)


class ade7912
{
public:
  ade7912();

  /**
   * @brief reconfigure_spi Configures CPOL and CPHA for this chip.
   */
  void
  reconfigure_spi()
  {
    // Turn on CPOL and CPHA
    SPCR |= (1 << CPOL) | (1 << CPHA);
    // Make sure MSB is transmitted first.
    SPCR &= ~_BV(DORD);
  }

  int16_t
  read_voltage_1();

private:
  bool
  write_register(uint8_t reg, uint8_t val)
  {

    // Make sure that val and check_val don't accidently match.
    uint8_t check_val = ~val;

    for (uint8_t attempts = 0; attempts < ADE7912_MAX_WRITE_ATTEMPTS;
         ++attempts)
    {
      // Do the write.
      select();
      spi_transcieve_byte(ADE7912_WRITE | reg);
      spi_transcieve_byte(val);
      deselect();

      // Check that it was written.
      select();
      spi_transcieve_byte(ADE7912_READ | reg);
      check_val = spi_transcieve_byte(ADE7912_DUMMY);
      deselect();

      if (val == check_val)
      {
        return true;
      }
    }

    return false;
  }

  uint8_t
  read_register(const uint8_t reg);

  void
  burst_read(const uint8_t reg, const uint8_t len, uint8_t* data);


  void
  select();

  void
  deselect();
};

#endif  // ADE7912_HPP
