#include "rfm9x.hpp"

rfm9x::rfm9x(uint8_t payload_length)
{
  deselect();
  DDRB |= _BV(PB2);  // Enable Chip Select pin as output.
  spi_init();

  // To enable LORA mode the device must be asleep.
  while (!set_mode(RFM96_REGOPMODE_LORAMODE | RFM96_REGOPMODE_SLEEP))
    ;

  // Set frequency to a 'Murican approved one.
  write_register(RFM96_REGFRLSB, RFM96_REGFRLSB_915MHZ);
  write_register(RFM96_REGFRMID, RFM96_REGFRMID_915MHZ);
  write_register(RFM96_REGFRLSB, RFM96_REGFRLSB_915MHZ);

  // Max output power.
  write_register(RFM96_REGLNA, RFM96_REGLNA_MAXPOWER);
  // Configures output power with PA_BOOST.
  write_register(RFM96_REGPACONFIG,
                 RFM96_REGPACONFIG_MAXPOWER | RFM96_REGPACONFIG_PA_BOOST
                     | 0b1111);
  // REGPADAC must be set to 0x87 for high power operation (see Table 78).
  write_register(RFM96_REGPADAC, 0x87);


  // Set fixed packet size.
  write_register(RFM96_REGPACKETCONFIG1,
                 RFM96_REGPACKETCONFIG1_PACKETFORMAT_FIXEDLENGTH);
  // Writing it like this enables payloads up to 255 bytes.
  write_register(RFM96_REGPAYLOADLENGTH, payload_length);

  // Set FIFO base TX and RX addresses
  write_register(RFM96_REGFIFOTXBASEADDR, RFM96_REGFIFO_BASEADDR);
  write_register(RFM96_REGFIFORXBASEADDR, RFM96_REGFIFO_BASEADDR);

  // Enables DIO0/G0 interrupt to interrupt on DIO0 to trigger on TxDone.
  // See Table 63, DIO mapping LoRa mode.
  write_register(RFM96_REGDIOMAPPING1, 0b01000000);

  set_mode(RFM96_REGOPMODE_STDBY);
}

void
rfm9x::send(uint8_t const* data, uint8_t len)
{
  // WHAT IS GOING ON:
  // It seems that we're only able to enter TX mode after entering sleep,
  // standby, and then TX as opposed to going straight from standby to TX. For
  // that reason we're entering sleep momentarily below.

  while (!set_mode(RFM96_REGOPMODE_SLEEP))
    ;  // Wait until mode changes.
  while (!set_mode(RFM96_REGOPMODE_STDBY))
    ;  // Wait until mode changes.
  // Start FIFO at base address.
  write_register(RFM96_REGFIFOADDRPTR, RFM96_REGFIFO_BASEADDR);

  // Write data
  select();
  spi_transcieve_byte(RFM96_WRITE | RFM96_REGFIFO);
  for (uint8_t i = 0; i < len; ++i)
  {
    spi_transcieve_byte(data[i]);
  }
  deselect();

  // Start transmitter.
  set_mode(RFM96_REGOPMODE_TX);

  return;
}

bool
rfm9x::set_mode(uint8_t mode)
{
  return write_register(RFM96_REGOPMODE, mode);
}

uint8_t
rfm9x::get_mode()
{
  return read_register(RFM96_REGOPMODE);
}

bool
rfm9x::write_register(const uint8_t reg, const uint8_t val)
{
  // Make sure that val and check_val don't accidently match.
  uint8_t check_val = ~val;

  for (uint8_t attempts = 0; attempts < RFM96_MAX_WRITE_ATTEMPTS; ++attempts)
  {
    // Do the write.
    select();
    spi_transcieve_byte(RFM96_WRITE | reg);
    spi_transcieve_byte(val);
    deselect();

    // Check that it was written.
    select();
    spi_transcieve_byte(RFM96_READ | reg);
    check_val = spi_transcieve_byte(RFM96_DUMMY);
    deselect();

    if (val == check_val)
    {
      return true;
    }
  }
  return false;
}


uint8_t
rfm9x::read_register(const uint8_t reg)
{
  select();
  spi_transcieve_byte(RFM96_READ | reg);
  auto val = spi_transcieve_byte(RFM96_DUMMY);
  deselect();
  return val;
}


uint8_t
rfm9x::get_status()
{
  return read_register(RFM96_REGMODEMSTAT);
}
