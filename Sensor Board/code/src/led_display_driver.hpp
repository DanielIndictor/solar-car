#ifndef LED_DISPLAY_DRIVER_HPP
#define LED_DISPLAY_DRIVER_HPP

#include "spi.hpp"

#include <avr/io.h>

// Same as AS1107 and MAX7221.
#define MAX7219_PORT PORTB
#define MAX7219_DDR DDRB
#define MAX7219_CS 2

#define MAX7219_NOOP 0x0
// Convert digit to corresponding address.
#define MAX7219_DIG(n) (n + 1)
#define MAX7219_DECODEMODE 0x9
#define MAX7219_INTENSITY 0xA
#define MAX7219_SCANLIMIT 0xB
#define MAX7219_SHUTDOWN 0xC
#define MAX7219_DISPLAYTEST 0xF

/**
 * @brief The led_display_driver class manages (exactly) two cascaded led matrix
 * drivers. This includes the MAX7219/MAX7221 and their AS1106/AS1107 clones.
 */
class led_display_driver
{
public:
  led_display_driver();

  /**
   * @brief reconfigure_spi Configures CPOL and CPHA for this chip.
   */
  void
  reconfigure_spi()
  {
    // Turn off CPOL and CPHA
    SPCR &= ~((1 << CPOL) | (1 << CPHA));
    // Make sure MSB is transmitted first.
    SPCR &= ~_BV(DORD);
  }

  void
  write_digit(uint8_t digit, uint8_t val);

  // private:
  /**
   * @brief write_register
   * @param reg
   * @param val
   * @param driver Which driver to write to, 0-indexed.
   * @return
   */
  void
  write_register(uint8_t reg, uint8_t val, uint8_t driver = 0);

  void
  write_register_to_all(uint8_t reg, uint8_t val);

  void
  select();

  void
  deselect();
};

#endif  // LED_DISPLAY_DRIVER_HPP
