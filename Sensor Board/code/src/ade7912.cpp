#include "ade7912.hpp"

#include "spi.hpp"

#include <avr/io.h>

ade7912::ade7912()
{
  spi_init();
  reconfigure_spi();

  // Configure slave select and DREADY as outputs and inputs, respectively.
  ADE7912_DDR |= (1 << ADE7912_CS);


  bool ade_setting_up = true;
  while (ade_setting_up)
  {
    ade_setting_up = read_register(ADE7912_STATUS0) & 1;
  }

  // 1ms sampling period and enable DREADY functionality.
  write_register(ADE7912_CONFIG, 0b00110000);
}

int16_t
ade7912::read_voltage_1()
{
  // Reading is 24 bits so we need 3 bytes.
  uint8_t reading[3];

  burst_read(ADE7912_V1WV, 3, reading);

  int16_t ret = 0;
  // reading now has Most Significant Byte in position 0 (and sign bit)
  ret |= static_cast<uint16_t>(reading[0]) << 8;
  // reading has next most significant byte in position 1.
  ret |= static_cast<uint16_t>(reading[0]);
  // Throw out last 8 bits because lazy.

  return ret;
}

uint8_t
ade7912::read_register(const uint8_t reg)
{
  select();
  spi_transcieve_byte(ADE7912_READ | reg);
  auto val = spi_transcieve_byte(ADE7912_DUMMY);
  deselect();
  return val;
}

void
ade7912::burst_read(const uint8_t reg, const uint8_t len, uint8_t* data)
{
  select();
  spi_transcieve_byte(ADE7912_READ | reg);
  for (uint8_t i = 0; i < len; ++i)
  {
    data[i] = spi_transcieve_byte(ADE7912_DUMMY);
  }
  deselect();
}

void
ade7912::select()
{
  ADE7912_PORT &= ~(1 << ADE7912_CS);
}

void
ade7912::deselect()
{
  ADE7912_PORT |= (1 << ADE7912_CS);
}
