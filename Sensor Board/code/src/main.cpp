//#include "ade7912.hpp"
//#include "internal_adc.hpp"
//#include "led_display_driver.hpp"
#include "rfm9x.hpp"
//#include "spi.hpp"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <stdint.h>
#include <util/delay.h>

// Make sure to reset early.
void
reset_watchdog_early(void) __attribute__((naked))
__attribute__((section(".init3")));
void
reset_watchdog_early(void)
{
  asm("WDR");
}

inline void
setup_led()
{
  DDRB |= _BV(PB1);
}

inline void
toggle_led()
{
  PORTB ^= _BV(PB1);
}

inline void
set_led(bool val)
{
  if (val)
  {
    PORTB &= ~_BV(PB1);
  }
  else
  {
    PORTB |= _BV(PB1);
  }
}


bool
is_sender(void)
{
  DDRB &= ~_BV(PB0);  // Set PB0 as input
  PORTB |= _BV(PB0);  // Set PB0 internal pull-up
  return (PINB & _BV(PB0));
}

inline void
setup_interrupts()
{
  EICRA |=
      _BV(ISC00) | _BV(ISC01);  // Configure INT0 to trigger on rising edge.
  EIMSK |= _BV(INT0);  // Enable INT0
}

const uint8_t data[] {"asdf"};
bool data_ready = false;


int
main()
{
  //  wdt_reset();
  //  wdt_enable(WDTO_4S);  // Keep WDT handling code at beginning.

  // Power off TWI, all timer/counters, and USART.
  //  PRR |= _BV(PRTWI) | _BV(PRTIM0) | _BV(PRTIM1) | _BV(PRTIM2) |
  //  _BV(PRUSART0);

  //  spi_init();
  //  SPCR |= _BV(SPR0) | _BV(SPR1);  // Slow down SPI to Fosc/128 frequency.

  setup_led();

  setup_interrupts();
  sei();  // Enable interrupts.

  //  wdt_reset();  // Reset after successful setup.

  _delay_ms(3000);
  rfm9x radio(4);
  while (1)
  {
    //    toggle_led();
    //        wdt_reset();
    radio.send(data, 4);
    _delay_ms(1000);
  }
}

ISR(INT0_vect)
{
  data_ready = true;
  toggle_led();
}
