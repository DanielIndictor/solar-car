#include "internal_adc.hpp"

#include <avr/io.h>

internal_adc::internal_adc()
{
  // Initialize ADC

  // Section "Prescaling and Conversion Timing" dictates that the ADC clock
  // divider must be set in such a way that the input clock to the ADC is
  // between 50kHZ and 200kHz. If the device is running above 6.4 MHZ (and below
  // the maximum operating frequency), setting the divider to be 1/128 of F_CPU
  // will result in an appropriate ADC clock.
#if (F_CPU >= 6400000)
  ADCSRA |= _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);
#else
#  error \
      "You must make sure that the ADC clock divider is set up correctly for your clock frequency."
#endif
  // Set reference to internal 1.1V reference.
  ADMUX |= _BV(REFS0) | _BV(REFS1);

  // Enable ADC
  ADCSRA |= _BV(ADEN);
}

void
internal_adc::begin_voltage_reading()
{
  ADCSRA |= _BV(ADSC);
}

uint16_t
internal_adc::await_reading()
{
  // Wait until conversion done.
  while (ADCSRA & _BV(ADSC))
    ;
  // Get readings.
  uint16_t ret = ADCL;
  ret |= (static_cast<uint16_t>(ADCH) << 8);
  return ret;
}
