Welcome to the repository of all code associated with the Staten Island Solar Car Program! In [Failemetry](./Failemetry) is the code written to do telemetry in the old system used in summer 2019. In [CarCircuit](./CarCircuit) are the schematics for the current solar car (WIP). In [Sensor Board](./Sensor Board) are the code and plans for the new telemetry system.

