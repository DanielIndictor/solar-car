import sqlite3
import os

conn = sqlite3.connect('failemetry.db')

c = conn.cursor()

c.execute('''CREATE TABLE if not exists readings
             (timestamp integer unique,
              filename text unique, 
              voltage real,
              current_in real,
              current_out real,
              speed real,
              PRIMARY KEY (timestamp)
              )''')


c.execute('''CREATE TABLE if not exists skipped_images
             (filename text)''')


if not os.path.exists('cached_images'):
    os.makedirs('cached_images')