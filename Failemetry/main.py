import sqlite3
import asyncio
import datetime
import os
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

SOLAR_CAR_FOLDER_ID = '1OYNY-YuEy4jXV2v5ff50iGgNG2o28aN6'
CACHED_IMAGE_PATH = 'cached_images'

def get_drive_interface():
    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()

    return GoogleDrive(gauth)

def main():
    # drive = get_drive_interface()
    #
    # # new_pictures = drive.ListFile(
    # #     {'q': "'{}' in parents".format(SOLAR_CAR_FOLDER_ID),
    # #      'orderBy': 'title desc'}).GetList()
    # new_pictures = drive.ListFile({'q': 'mimeType=\'image/jpeg\'',
    #                                'spaces': 'drive'}).GetList()
    # for picture in new_pictures:
    #     print('title: {}, id: {}'.format(picture['title'], picture['id']))


    # cached_filenames = [f for
    #                  f in os.listdir(CACHED_IMAGE_PATH)
    #                  if os.path.isfile(os.path.join(CACHED_IMAGE_PATH, f))]
    # # Take the names out of the tuple.
    # print(cached_filenames)
    #
    conn = sqlite3.connect('failemetry.db')
    c = conn.cursor()
    c.execute('select filename from readings')
    database_filenames = c.fetchall()
    database_filenames = [tuple_[0] for tuple_ in database_filenames]
    print(database_filenames)
    date_object = extract_date_from_title('IMG_20090709_234618.jpg')
    print(date_object.tzinfo)
    print(extract_date_from_title('IMG_20090709_234618.jpg').timestamp())


def extract_date_from_title(title: str):
    """Given a filename like IMG_20190708_034654.jpg returns a datetime 
    representative of it."""
    ret = datetime.datetime(year=int(title[4:8]),
                            month=int(title[8:10]),
                            day=int(title[10:12]),
                            hour=int(title[13:15]),
                            minute=int(title[15:17]),
                            second=int(title[17:19]))
    # ret.tzinfo = datetime.timezone.
    return ret

def extract_date_from_timestamp(timestamp: int):
    """Converts a unix timestamp to a datetime in UTC."""
    return datetime.datetime.utcfromtimestamp(timestamp)

if __name__ == '__main__':
    main()