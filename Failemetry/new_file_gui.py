from PyQt5 import QtCore, QtGui, QtWidgets
import datetime
import threading
import sqlite3
import asyncio
import datetime
import os
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

SOLAR_CAR_FOLDER_ID = '1OYNY-YuEy4jXV2v5ff50iGgNG2o28aN6'
CACHED_IMAGE_PATH = 'cached_images'

def get_drive_interface():
    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()
    return GoogleDrive(gauth)

def extract_date_from_title(title: str):
    """Given a filename like IMG_20190708_034654.jpg returns a datetime
    representative of it."""
    ret = datetime.datetime(year=int(title[4:8]),
                            month=int(title[8:10]),
                            day=int(title[10:12]),
                            hour=int(title[13:15]),
                            minute=int(title[15:17]),
                            second=int(title[17:19]))
    # ret.tzinfo = datetime.timezone.
    return ret

def _font_of_size(size):
    """
    Convenience function to automatically generate QFonts of an intended size.
    :param size: Size of new font.
    :return: QFont of given point size.
    """
    ret = QtGui.QFont()
    ret.setPointSize(size)
    return ret


class ValueAssigner(QtWidgets.QMainWindow):
    SCALE_UP = 1.1
    SCALE_DOWN = 1/SCALE_UP
    UPDATE_INTERVAL = 7000
    NO_IMAGES_TO_SHOW_TEXT = ''

    @staticmethod
    def _font_of_size(size):
        """
        Convenience function to automatically generate QFonts of an intended size.
        :param size: Size of new font.
        :return: QFont of given point size.
        """
        ret = QtGui.QFont()
        ret.setPointSize(size)
        return ret

    def __init__(self):
        super().__init__()
        # Get this done now so nothing breaks later.
        self.update_thread = None

        self.database_lock = threading.Lock()
        # Setup gui.
        self._w = {}  # Stores widgets.
        self._w['central'] = QtWidgets.QWidget(self)
        self.setCentralWidget(self._w['central'])
        self._w['central'].setLayout(QtWidgets.QVBoxLayout())
        self._w['central_l'] = self._w['central'].layout()

        # Scroll bar + Label combo inspired by
        # https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
        self._w['image_label'] = QtWidgets.QLabel()
        self._w['image_label'].setBackgroundRole(QtGui.QPalette.Base)
        self._w['image_label'].setScaledContents(True)
        self._w['image_label'].setSizePolicy(
            QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum,
                                  QtWidgets.QSizePolicy.Maximum))

        self._w['image_scroll'] = QtWidgets.QScrollArea()
        self._w['image_scroll'].setBackgroundRole(QtGui.QPalette.Dark)
        self._w['image_scroll'].setWidget(self._w['image_label'])
        self._w['image_scroll'].setWidgetResizable(True)
        self._w['image_scroll'].setVisible(True)
        self._w['central_l'].addWidget(self._w['image_scroll'])

        self._w['form_layout'] = QtWidgets.QHBoxLayout()
        self._w['central_l'].addLayout(self._w['form_layout'])
        # Add the labels/textboxes.
        label_font = textbox_font = self._font_of_size(20)
        label_font = self._font_of_size(18)
        for widget_name, text in [('file_name', 'File Name'),
                                  ('voltage', 'Voltage Reading'),
                                  ('current_in', 'Current In (Amps)'),
                                  ('current_out', 'Current Out (Amps)'),
                                  ('speed', 'Speed')]:

            self._w[widget_name + '_label'] = QtWidgets.QLabel()
            self._w[widget_name + '_label'].setFont(label_font)
            self._w[widget_name + '_label'].setText(text)
            self._w[widget_name + '_textbox'] = QtWidgets.QLineEdit()
            self._w[widget_name + '_textbox'].setFont(textbox_font)
            self._w[widget_name + '_layout'] = QtWidgets.QVBoxLayout()
            self._w[widget_name + '_layout'].addWidget(
                self._w[widget_name + '_label'])
            self._w[widget_name + '_layout'].addWidget(
                self._w[widget_name + '_textbox'])
            self._w['form_layout'].addLayout(self._w[widget_name + '_layout'])

        self._w['file_name_textbox'].setMinimumWidth(360)
        self._w['form_submission_layout'] = QtWidgets.QHBoxLayout()
        self._w['images_left_label'] = QtWidgets.QLabel()
        self._w['images_left_label'].setFont(label_font)
        self._w['form_submission_layout'].addWidget(
            self._w['images_left_label'])
        self._w['form_submission_spacer'] = \
            QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding,
                                  QtWidgets.QSizePolicy.Minimum)
        self._w['form_submission_layout'].addItem(
            self._w['form_submission_spacer'])
        self._w['skip_button'] = QtWidgets.QPushButton()
        self._w['skip_button'].setText('Skip and Discard')
        self._w['skip_button'].setFont(textbox_font)
        self._w['skip_button'].clicked.connect(self.skip)
        self._w['form_submission_layout'].addWidget(self._w['skip_button'])
        self._w['submit_button'] = QtWidgets.QPushButton()
        self._w['submit_button'].setText('Submit')
        self._w['submit_button'].setFont(textbox_font)
        self._w['submit_button'].clicked.connect(self.submit)

        self._w['form_submission_layout'].addWidget(self._w['submit_button'])
        self._w['central_l'].addLayout(self._w['form_submission_layout'])

        # These will be automatically filled in.
        self._w['file_name_textbox'].setReadOnly(True)

        self._w['status_bar'] = QtWidgets.QLabel()
        self._w['status_bar'].setFont(self._font_of_size(12))
        self._w['central_l'].addWidget(self._w['status_bar'])
        self.status = ''

        # Note that self.check_for_new_images depends on self.unseen_images
        # being defined.
        self.unseen_image_titles = set()

        self._timer = QtCore.QTimer()
        self._timer.setInterval(self.UPDATE_INTERVAL)
        self._timer.timeout.connect(self.check_for_new_images)
        self._timer.start()

        # Set up database stuff.
        self.conn = sqlite3.connect('failemetry.db',
                                    check_same_thread=False)

        # Set up Google Drive.
        self.drive = get_drive_interface()

        # Set up default
        self.set_image('img.jpg')
        self._w['file_name_textbox'].setText(self.NO_IMAGES_TO_SHOW_TEXT)

    @property
    def cached_image_titles(self):
        return [f for
                f in os.listdir(CACHED_IMAGE_PATH)
                if os.path.isfile(os.path.join(CACHED_IMAGE_PATH, f))]

    @property
    def skipped_image_titles(self):
        c = self.conn.cursor()  # Make sure to get a cursor from current thread.
        c.execute('select filename from skipped_images')
        return [tuple_[0] for tuple_ in c.fetchall()]

    @property
    def processed_image_titles(self):
        c = self.conn.cursor()
        c.execute('select filename from readings')
        return [tuple_[0] for tuple_ in c.fetchall()]

    @property
    def status(self):
        return self._w['status_bar'].text()

    @status.setter
    def status(self, text):
        return self._w['status_bar'].setText(
            datetime.datetime.now().replace(microsecond=0).isoformat()
            + ' ' + text)

    @property
    def current_image_title(self):
        return self._w['file_name_textbox'].text()

    @current_image_title.setter
    def current_image_title(self, title):
        return self._w['file_name_textbox'].setText(title)

    @property
    def unseen_image_titles(self):
        self._w['images_left_label'].setText(
            '{} images left.'.format(len(self._unseen_images)))
        return self._unseen_images

    @unseen_image_titles.setter
    def unseen_image_titles(self, value):
        self._unseen_images = value
        self._w['images_left_label'].setText(
            '{} images left.'.format(len(value)))

    def set_image(self, path):
        '''Show the image in the scroll area.'''

        # reader = QtGui.QImageReader(path)
        # reader.setAutoTransform(True)
        # image = reader.read()
        # print(image.isNull())
        # self._w['image_label'].setPixmap(QtGui.QPixmap.fromImage(image))

        self._w['image_label'].setPixmap(QtGui.QPixmap(path))
        self._w['image_label'].show()

    def check_for_new_images(self):
        """Looks for new images and adds them to the queue in another thread."""
        # MUST NOT WRITE TO DATABASE SINCE WE'RE ACCESSING DB CONNECTION
        # FROM DIFFERENT THREAD.
        # self.update_thread will be None if this function is being called for
        # the first time. Otherwise we won't interrupt if it's running.
        if self.update_thread is None:
            self.update_thread = threading.Thread(
                target=lambda: self._check_for_new_images())
            self.update_thread.start()
        elif not self.update_thread.is_alive():
            # If the thread has already updated we'll update again.
            self.update_thread.join()
            self.update_thread = threading.Thread(
                target=lambda: self._check_for_new_images())
            self.update_thread.start()


    def _check_for_new_images(self):
        """Does the actual work of updating."""

        # Gonna load these ahead of time since each access
        # checks the database/filesystem.
        with self.database_lock:
            processed_image_titles = self.processed_image_titles
            skipped_image_titles = self.skipped_image_titles
            cached_image_titles = self.cached_image_titles
            # While self.unseen_image_titles isn't connected with the database
            # we want all users of these image titles to have access to them at
            # the same point in time.
            unseen_image_titles = self.unseen_image_titles

            # See PyDrive documentation for how this works
            # but basically it's a generator of file lists.
            drive_pictures_generator = self.drive.ListFile(
                {'q': "'{}' in parents and mimeType='image/jpeg'".format(
                    SOLAR_CAR_FOLDER_ID),
                    'orderBy': 'title desc',
                    'maxResults': 200,
                    'fields': 'items(id,title,downloadUrl)'})

            # Keep paging through Drive pictures until we find a
            # set that we have already seen..
            for drive_pictures in drive_pictures_generator:
                found_new_image = False
                for picture in drive_pictures:
                    if picture['title'] not in unseen_image_titles \
                            and picture['title'] not in processed_image_titles \
                            and picture['title'] not in skipped_image_titles:
                        found_new_image = True
                        # If we're gonna be adding the image we should also save it.
                        if picture['title'] not in cached_image_titles:
                            picture.GetContentFile(os.path.join(CACHED_IMAGE_PATH,
                                                                picture['title']))
                            self.status = 'Downloaded \'{}\''.format(picture['title'])

                        # Regardless of whether or not we already downloaded it
                        # we must still add it to our list.
                        self.unseen_image_titles.add(picture['title'])

                if not found_new_image:
                    break

    def submit(self):
        # Early quit if there's nothing to submit.
        with self.database_lock:
            if len(self.unseen_image_titles) == 0:
                self.reset_image_viewer()
                return
            elif self.current_image_title == '':  # Then just load next image
                self.load_next_image()
                return


        image_datetime = extract_date_from_title(self.current_image_title)
        try:
            voltage = float(self._w['voltage_textbox'].text())
            current_in = float(self._w['current_in_textbox'].text())
            current_out = float(self._w['current_out_textbox'].text())
            speed = float(self._w['speed_textbox'].text())
        except ValueError:
            self.status = 'Try again!'
            return

        sql = ''' INSERT INTO readings ''' \
              '''(timestamp,filename,voltage,current_in,current_out,speed) ''' \
              '''VALUES(?,?,?,?,?,?)'''

        with self.database_lock:
            self.conn.cursor().execute(sql,
                                       (
                                           int(image_datetime.timestamp()),
                                           self.current_image_title,
                                           voltage,
                                           current_in,
                                           current_out,
                                           speed
                                       )
                                       )
            self.conn.commit()
            self.load_next_image()
        self.status = 'Submitted new image.'

    def load_next_image(self):
        if len(self.unseen_image_titles) == 0:
            self.reset_image_viewer()
        else:
            current_image_title = self.unseen_image_titles.pop()
            self.current_image_title = current_image_title
            self.set_image(os.path.join(CACHED_IMAGE_PATH,
                                        self.current_image_title))

    def skip(self):
        # Early quit if there's nothing to skip.
        if len(self.unseen_image_titles) == 0:
            self.reset_image_viewer()
            return
        elif self.current_image_title == '':  # Then just load next image
            with self.database_lock:
                self.load_next_image()
            return

        sql = ''' INSERT INTO skipped_images ''' \
              '''(filename) ''' \
              '''VALUES(?)'''
        with self.database_lock:
            self.conn.cursor().execute(sql,
                                       (self.current_image_title,)
                                       )
            self.conn.commit()
            self.load_next_image()
        self.status = 'Skipped image!'

    def reset_image_viewer(self):
        self.set_image('img.jpg')
        self.current_image_title = self.NO_IMAGES_TO_SHOW_TEXT
        self._w['current_in_textbox'].setText('')
        self._w['current_out_textbox'].setText('')
        self._w['speed_textbox'].setText('')

if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    application = ValueAssigner()
    application.show()
    sys.exit(app.exec_())

