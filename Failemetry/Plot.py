import numpy as np
import pandas as pd
pd.plotting.register_matplotlib_converters(explicit=True)  # Future warning w/o this line
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib
matplotlib.use( 'tkagg' ) # Force GUI
import sqlite3
from datetime import datetime


def to_seconds(days = None, hours = None, minutes = None):
    result = 0
    if days is not None:
        result += days * 86400
    if hours is not None:
        result += hours * 3600
    if minutes is not None:
        result += minutes * 60
    return result


view_window = [0, 12, 0] # Days, hours, minutes

if __name__ == "__main__":

    print("[Plot] Connecting to database...")

    try:
        conn = sqlite3.connect('failemetry.db')
    except sqlite3.Error as e:
        print("[Plot] Exception in connecting to database: ", str(e))

    try:
        # Init data
        length = 0
        print("[Plot] Waiting for data...")
        while length < 2:
            sql = "SELECT Count(*) FROM readings WHERE \"timestamp\" >= {0}".format(int(datetime.now().timestamp()) -
                                                                                    to_seconds(*view_window))
            cur = conn.cursor()
            le = cur.execute(sql)
            for row in cur:
                length = row[0]
            time.sleep(0.05)

        print("[Plot] Starting plotter  ...")

        # TODO: Display integral curve

        fig = plt.figure()
        ax1 = fig.add_subplot(411)
        ax2 = ax1.twinx()
        ax3 = fig.add_subplot(412)
        ax4 = fig.add_subplot(413)
        ax5 = fig.add_subplot(414)

        fig.canvas.set_window_title("Solar Car Telemetry Data")
        print("Data:")

        energy_readings = {}

        def animate(interval):
            # Data input
            df = pd.read_sql_query("SELECT * FROM readings WHERE \"timestamp\" >= {0} ORDER BY timestamp".format(int(datetime.now().timestamp()) -
                                                                                                                 to_seconds(*view_window)), conn)
            df['input_time'] = pd.to_datetime(df['timestamp'], unit='s')
            df['power_in'] = df['current_in'] * df['voltage']
            df['power_out'] = df['current_out'] * df['voltage']
            df['net_power'] = df['power_in'] - df['power_out']

            ax1.clear()
            ax2.clear()
            ax3.clear()
            ax4.clear()

            # Plot 1
            color = 'tab:blue'
            ax1.set_xlabel('Time')
            ax1.set_ylabel('Voltage (V)', color=color)
            line0, = ax1.plot(df['input_time'], df['voltage'], color=color, label='Voltage')
            ax1.legend()

            ax1.tick_params(axis='y', labelcolor=color)
            ax1.tick_params(labelbottom=False)
            ax1.xaxis.label.set_visible(False)
            ax1.tick_params(axis="x", direction="in")

            color = 'tab:red'
            ax2.set_ylabel('Current (A)', color=color)
            line1, = ax2.plot(df['input_time'], df['current_in'], color=color, label='Current In')
            line2, = ax2.plot(df['input_time'], df['current_out'], color=color, linestyle='--', label='Current Out')
            ax2.legend()
            ax2.tick_params(axis='y', labelcolor=color)

            # Plot 2
            ax3.set_xlabel('Time')
            ax3.set_ylabel('Power (W)', color=color)
            line3, = ax3.plot(df['input_time'], df['power_in'], color='tab:purple', label='Power In')
            line4, = ax3.plot(df['input_time'], df['power_out'], color='tab:cyan', label='Power Out')
            ax3.legend()
            ax3.tick_params(labelbottom=False)
            ax3.xaxis.label.set_visible(False)
            ax3.tick_params(axis="x", direction="in")

            # Plot 3
            color = 'tab:green'
            ax4.set_xlabel('Time')
            ax4.set_ylabel('Net Power (W)', color=color)
            line5, = ax4.plot(df['input_time'], df['net_power'], color=color, label='Net Power')
            ax4.tick_params(labelbottom=False)
            ax4.xaxis.label.set_visible(False)
            ax4.tick_params(axis="x", direction="in")


            # Plot 4
            color = 'tab:brown'
            ax5.set_xlabel('Time')
            ax5.set_ylabel('Speed (mph)', color=color)
            line5, = ax5.plot(df['input_time'], df['speed'], color=color, label='Speed')

            plt.setp(ax5.xaxis.get_ticklabels(), rotation=70)

            plt.tight_layout()
            plt.subplots_adjust(bottom=0.20, hspace=0)

            # HERE THERE BE EPISTEMIC DRAGONS! BEWARE!

            now = int(datetime.now().timestamp())
            times_to_be_checked = [300, 600, 1800, 3600, 10800]
            for time_window in times_to_be_checked:
                windowed_df = df[df['timestamp'] >= (now - time_window)]
                dt = np.diff(windowed_df['timestamp'])
                power = windowed_df['net_power']

                energy_readings[time_window//60] = np.sum(dt * np.add(power[1:], power[:-1])) / 2

            # Hic sunt dracones plenius esse

            formatted_energy_readings = \
                '{' + ', '.join(['{}: {}'.format(k, round(v, 3)) for k, v in energy_readings.items()]) + '}'

            print('V: {} | P_i: {} | P_o {} | P_n:  {} | MPH: {} | Energy: {}'.format(
                round(df['voltage'][df.index[-1]], 3),
                 round(df['power_in'][df.index[-1]], 3),
                 round(df['power_out'][df.index[-1]], 3),
                 round(df['net_power'][df.index[-1]], 3),
                 str(df['speed'][df.index[-1]]),
                 formatted_energy_readings
                           ))

        ani = animation.FuncAnimation(fig, animate, interval=500)
        plt.show()

    except KeyboardInterrupt:
        print("[Plot] Stopping...")
        conn.close()
        plt.close()
